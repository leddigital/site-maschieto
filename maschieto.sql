-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: maschieto
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `info1` text,
  `info2` text,
  `link1` text,
  `path` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `position` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `textalign` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (17,'banner_5dcbe9292b6f3.jpeg','Primeiro Slide','Sofá Folha','Design premiado e modernidade para ambientes',NULL,NULL,'2019-11-13 11:29:45','2019-11-13 11:29:45',NULL,NULL,'header','#494949','center-right'),(18,'banner_5dcbe969d7fa1.jpeg','Segundo Slide','Invista em ambientes contemporâneos',NULL,'javascript:;',NULL,'2019-11-13 11:30:49','2019-11-13 11:30:49',NULL,NULL,'header','#ffffff','center-right'),(19,'banner_5dcbe9bfe4f49.jpeg','Terceiro Slide','Produção com tecnologia que garante móveis com acabamentos perfeitos',NULL,NULL,NULL,'2019-11-13 11:32:15','2019-11-13 11:32:15',NULL,NULL,'header','#494949','slider-caption-center'),(20,'banner_5dcc23a61507a.jpeg','Primeiro Banner Rodapé',NULL,NULL,NULL,NULL,'2019-11-13 15:39:18','2019-11-13 15:39:18',NULL,NULL,'footer',NULL,'top-left'),(21,'banner_5dcc23b5bc89a.jpeg','Segundo Banner Footer',NULL,NULL,NULL,NULL,'2019-11-13 15:39:33','2019-11-13 15:39:33',NULL,NULL,'footer',NULL,'top-left'),(22,'banner_5dcc23cadd6ad.jpeg','Terceiro Slide Rodapé',NULL,NULL,NULL,NULL,'2019-11-13 15:39:54','2019-11-13 15:39:54',NULL,NULL,'footer',NULL,'top-left'),(23,'banner_5dcc23db0f82f.jpeg','Quarto Slide Rodapé',NULL,NULL,NULL,NULL,'2019-11-13 15:40:11','2019-11-13 15:40:11',NULL,NULL,'footer',NULL,'top-left'),(24,'banner_5dcc23eadec36.jpeg','Quinto Slide Rodapé',NULL,NULL,NULL,NULL,'2019-11-13 15:40:26','2019-11-13 15:40:26',NULL,NULL,'footer',NULL,'top-left');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `short_description` text,
  `image` varchar(255) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (14,'Mesas','mesas',NULL,'asd as das as dsa dsa as sa','categoria_5dcdbb0869744.jpeg',1),(15,'Aparadores','aparadores',NULL,'sdf sdf sd sfd fsd fsd fsd fsd','categoria_5dcdbb1ccc45d.jpeg',1),(16,'Sofás','sofas',NULL,'sdf sd fsd sdf sd sd sd sd fsdsd','categoria_5dcdbb2cd4eab.jpeg',1),(17,'Bar','bar',NULL,'s dfsad fsd sdf fsd','categoria_5dcdbb40689ce.jpeg',0);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_has_contents`
--

DROP TABLE IF EXISTS `categories_has_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_has_contents` (
  `categories_id` int(11) NOT NULL,
  `contents_id` int(11) NOT NULL,
  PRIMARY KEY (`categories_id`,`contents_id`),
  KEY `fk_categories_has_contents_contents1_idx` (`contents_id`),
  KEY `fk_categories_has_contents_categories1_idx` (`categories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_has_contents`
--

LOCK TABLES `categories_has_contents` WRITE;
/*!40000 ALTER TABLE `categories_has_contents` DISABLE KEYS */;
INSERT INTO `categories_has_contents` VALUES (8,42),(14,43);
/*!40000 ALTER TABLE `categories_has_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `short_description` text,
  `content` longtext,
  `type` varchar(45) NOT NULL COMMENT 'post,news,page,page-name',
  `url` varchar(45) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `email_contato` text,
  `email_assunto` text,
  `video` text,
  `descritivo` text,
  `featured` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_UNIQUE` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents`
--

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
INSERT INTO `contents` VALUES (6,'SOBRE A MASCHIETO',NULL,'Nossa indústria foi fundada em 1968 com o propósito de realizar o desejo das pessoas em encontrar móveis que completam ambientes com estilo, design e conforto.','<p>Nossa indústria foi fundada em 1968 com o propósito de realizar o desejo das pessoas em encontrar móveis que completam ambientes com estilo, design e conforto.</p><p>A qualidade fica evidente em cada etapa do nosso processo fabricação. Da compra da matéria-prima com certificação ambiental à produção. Do controle de qualidade a expedição. Desenhamos peças que acompanham as tendências do design contemporâneo.</p>','sobre','sobre-a-maschieto','15737592575dcda919bd446.jpeg','2019-11-13 14:12:23','2019-11-14 19:20:57',NULL,NULL,'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/0zAOkBVE0D4\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>',NULL,NULL),(7,'TRABALHE NA MASCHIETO',NULL,'Se você tem talento e comprometimento no que faz, queremos falar com você. Envie seu currículo e aguarde nosso contato.',NULL,'trabalhe','trabalhe-na-maschieto',NULL,'2019-11-13 14:52:22','2019-11-14 15:24:57','contato@moveismaschieto.com.br','Vaga | Maschieto',NULL,NULL,NULL),(8,'SEJA UMA REVENDA MASCHIETO',NULL,'Trabalhe com a qualidade e conceito dos Móveis Maschieto! Preencha o formulário abaixo. Entraremos em contato.',NULL,'revendedor','seja-uma-revenda-maschieto',NULL,'2019-11-13 15:23:52','2019-11-13 15:30:35',NULL,NULL,NULL,NULL,NULL),(12,'ENTRE EM CONTATO',NULL,'Preencha o formulário abaixo. Entraremos em contato.',NULL,'contato','entre-em-contato',NULL,'2019-11-13 16:58:43','2019-11-13 17:00:57',NULL,NULL,NULL,NULL,NULL),(17,'Dúvidas, críticas ou sugestões?',NULL,'Atendemos de segunda a sexta, das 7h às 11h30 e das 13h às 17h18.','ENTRE EM CONTATO','chamada-contato','duvidas,-criticas-ou-sugestoes?',NULL,'2019-11-13 17:57:54','2019-11-13 18:12:11',NULL,NULL,NULL,NULL,NULL),(18,'SEJA UM REVENDEDOR',NULL,'Trabalhe com os produtos Maschieto e aumente as oportunidades de negócios na sua loja.','QUERO VENDER','chamada-revenda','seja-um-revendedor',NULL,'2019-11-13 17:58:26','2019-11-14 15:27:29',NULL,NULL,NULL,NULL,NULL),(19,'Trabalhe Conosco',NULL,'Se você tem talento e comprometimento no que faz, queremos falar com você.','ENVIE SEU CURRÍCULO','chamada-trabalhe','trabalhe-conosco',NULL,'2019-11-13 17:58:40','2019-11-13 18:10:39',NULL,NULL,NULL,NULL,NULL),(43,'Mesa Camila',NULL,NULL,'<p>as da dsa dsa sad asd dsa das d&nbsp;</p>','produto','mesa-camila','produto_5dcdbb7246a08.jpeg','2019-11-14 20:39:14','2019-11-14 20:39:14',NULL,NULL,NULL,'descritivo_5dcdbb72472a8.jpeg',1);
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_has_contents`
--

DROP TABLE IF EXISTS `contents_has_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_has_contents` (
  `contents_id` int(11) NOT NULL,
  `contents_child_id` int(11) NOT NULL,
  PRIMARY KEY (`contents_id`,`contents_child_id`),
  KEY `fk_contents_has_contents_contents2_idx` (`contents_child_id`),
  KEY `fk_contents_has_contents_contents1_idx` (`contents_id`),
  CONSTRAINT `fk_contents_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contents_has_contents_contents2` FOREIGN KEY (`contents_child_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_has_contents`
--

LOCK TABLES `contents_has_contents` WRITE;
/*!40000 ALTER TABLE `contents_has_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_has_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_images`
--

DROP TABLE IF EXISTS `contents_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `type` varchar(45) DEFAULT NULL,
  `order` varchar(45) DEFAULT NULL,
  `image` text,
  `contents_id` int(11) NOT NULL,
  `path` text,
  PRIMARY KEY (`id`,`contents_id`),
  KEY `fk_contents_images_contents_idx` (`contents_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_images`
--

LOCK TABLES `contents_images` WRITE;
/*!40000 ALTER TABLE `contents_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informations`
--

DROP TABLE IF EXISTS `informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` text,
  `number` varchar(45) DEFAULT NULL,
  `complement` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `whatsapp` varchar(45) DEFAULT NULL,
  `instagram` text,
  `facebook` text,
  `linkedin` text,
  `twitter` text,
  `pinterest` text,
  `phone1` varchar(45) DEFAULT NULL,
  `phone2` varchar(45) DEFAULT NULL,
  `phone3` varchar(45) DEFAULT NULL,
  `email1` text,
  `email2` text,
  `mailbox` text,
  `opening` text,
  `main_banner` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informations`
--

LOCK TABLES `informations` WRITE;
/*!40000 ALTER TABLE `informations` DISABLE KEYS */;
INSERT INTO `informations` VALUES (1,'Rod. Roberto Mario Perosa',NULL,'KM 11,5','Chácara São João','15860-000','Ibirá','SP',NULL,'https://www.instagram.com/moveis_maschieto','https://www.facebook.com/moveismaschieto',NULL,NULL,NULL,'(17) 3551-1064','(17) 3551-1261','(17) 3551-1407','contato@maschieto.com.br','moveis@maschieto.com.br','Caixa Postal 33','Segunda a sexta, das 7h às 11h30 e das 13h às 17h18','mainbanner_5dcaa76162f06.jpeg');
/*!40000 ALTER TABLE `informations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('enzo.nagata@gmail.com','$2y$10$F87xZsXNmV3On.AEylyz3esP.VH3c4P7mZUpv0atsKI7D9KyLMvXS','2019-09-13 21:50:52');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Enzo Nagata','enzo.nagata@gmail.com',NULL,'$2y$10$HUuXgtcUoSVCKRnGZ7g4WOIogDOepbY4ZRb5M2ZMyJHWSvK9fMD0W','t95qJAaDIFb8MAzlxNaRAyeJqAZcJB43SsC5Bz7c3woDaNyvLsVg6v0rOqfM','2019-08-23 14:40:17','2019-08-23 14:40:17'),(2,'Edinaldo','edinaldo@agencialed.com.br',NULL,'$2y$10$HYROoUsNkqgAz65RW0Ct4uEX9vDIMH8KlQlmrKHigyRNfYUwzOUvG',NULL,'2019-09-13 22:19:59','2019-09-13 22:19:59'),(3,'Digital','digital@agencialed.com.br',NULL,'$2y$10$n5jPIIeqG9CNR3HOzaTIC.fDq7Rm2FPjSI39DEspDNKnu98KHu2HW',NULL,'2019-11-11 15:31:40','2019-11-11 15:31:40');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'maschieto'
--

--
-- Dumping routines for database 'maschieto'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-18  8:24:57
