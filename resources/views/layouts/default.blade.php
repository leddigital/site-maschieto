<!DOCTYPE html>
<html dir="ltr" lang="pt-BR">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <link
        href="http://fonts.googleapis.com/css?family=Mukta+Vaani:300,400,500,600,700|Open+Sans:300,400,600,700,800,900"
        rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/dark.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/swiper.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('css/components/bs-select.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('css/car.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/car-icons/style.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('css/font-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="{{ asset('css/colors.php@color=c85e51.css') }}" type="text/css" />

    <link rel="stylesheet" type="text/css" href="{{ asset('css/settings.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/layers.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/navigation.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/lightbox.css') }}">

    <title>Maschieto | Móveis Planejados</title>
    <style>
        /* Revolution Slider */
        .ares .tp-tab {
            border: 1px solid #333;
        }

        .ares .tp-tab-content {
            margin-top: -4px;
        }

        .ares .tp-tab-content {
            padding: 15px 15px 15px 110px;
        }

        .ares .tp-tab-image {
            width: 80px;
            height: 80px;
        }

    </style>
</head>

<body class="stretched side-push-panel"
    {{-- data-loader-html="<div><img src='{{ asset('images/page-loader.gif') }}' alt='Loader' --}}></div>

    <div class="body-overlay"></div>

    <div id="side-panel">
        <div id="side-panel-trigger-close" class="side-panel-trigger"><a href="javascript:;"><i
                    class="icon-line-cross"></i></a></div>
        <div class="side-panel-wrap">
            <div class="widget clearfix">
                <a href="javascript:;"><img src="{{ asset('images/logo@2x.jpg') }}" alt="Canvas Logo" height="50"></a>
                <p>Curabitur varius magna a eros iaculis, et scelerisque tellus pulvinar. Duis condimentum iaculis
                    metus, nec condimentum lorem porta commodo. Mauris justo est, lobortis id vestibulum.</p>
                <div class="widget quick-contact-widget form-widget noborder notoppadding clearfix">
                    <h4>Quick Contact</h4>
                    <div class="form-result"></div>
                    <form id="quick-contact-form" name="quick-contact-form"
                        action="http://themes.semicolonweb.com/html/canvas/include/form.php" method="post"
                        class="quick-contact-form nobottommargin">
                        <div class="form-process"></div>
                        <input type="text" class="required sm-form-control input-block-level"
                            id="quick-contact-form-name" name="quick-contact-form-name" value=""
                            placeholder="Full Name" />
                        <input type="text" class="required sm-form-control email input-block-level"
                            id="quick-contact-form-email" name="quick-contact-form-email" value=""
                            placeholder="Email Address" />
                        <textarea class="required sm-form-control input-block-level short-textarea"
                            id="quick-contact-form-message" name="quick-contact-form-message" rows="4" cols="30"
                            placeholder="Message"></textarea>
                        <input type="text" class="hidden" id="quick-contact-form-botcheck"
                            name="quick-contact-form-botcheck" value="" />
                        <input type="hidden" name="prefix" value="quick-contact-form-">
                        <button type="submit" id="quick-contact-form-submit" name="quick-contact-form-submit"
                            class="button button-small button-3d nomargin" value="submit">Send Email</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="wrapper" class="clearfix">

        <header id="header" class="static-sticky full-header clearfix">
            <div id="header-wrap">
                <div class="container-fluid clearfix">
                    <div id="primary-menu-trigger">
                        <i class="icon-reorder"></i>
                    </div>
                    <div id="logo">
                        <a href="{{ route('nav.index') }}" class="standard-logo"><img
                                src="{{ asset('images/logo.jpg') }}" alt="Mascheito"></a>
                        <a href="{{ route('nav.index') }}" class="retina-logo"><img src="{{ asset('images/logo.jpg') }}"
                                alt="Mascheito"></a>
                    </div>
                    <nav id="primary-menu" class="with-arrows clearfix">
                        <ul class="list-wrapper-menu" class="d-flex align-items-center">
                            {{-- <li class="current d-flex p-2">
                                <a href="javascript:;">
                                    <div>Home</div>
                                </a>
                            </li> --}}
                            <li class="d-flex p-2">
                                <a href="{{ route('nav.sobre') }}">
                                    <div>A Maschieto</div>
                                </a>
                            </li>
                            <li class="mega-menu d-flex p-2">
                                <a style="padding: 40% 0%;" href="{{ route('nav.produtos') }}">
                                    <div>Produtos</div>
                                </a>
                                <div class="mega-menu-content style-2 clearfix">
                                    <ul class="mega-menu-column col-lg-12">
                                        <li>
                                            <div class="widget center clearfix">
                                                {{-- <h3 class="nobottommargin">Produtos em destaque</h3> --}}
                                                <a href="{{ route('nav.produtos') }}"
                                                    class="button button-small button-rounded button-border button-dark button-black font-primary"
                                                    style="margin: 10px 0 40px">Ver todos os produtos</a>

                                                <div class="owl-carousel customjs image-carousel carousel-widget"
                                                    data-margin="20" data-nav="false" data-pagi="true" data-items-xs="1"
                                                    data-items-sm="2" data-items-md="4" data-items-lg="6"
                                                    data-items-xl="6">

                                                    @foreach ($categories as $category)

                                                        <div class="oc-item center">
                                                            <a href="{{ route('nav.produtos') }}">
                                                            <img src="{{ asset('categories/'.$category->image) }}" alt="{{ $category->title }}">
                                                                <h5>{{ $category->title }}</h5>
                                                            </a>
                                                        </div>

                                                    @endforeach

                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="d-flex p-2">
                                <a href="{{ route('nav.revenda') }}">
                                    <div>Seja um revendedor</div>
                                </a>
                            </li>
                            <li class="d-flex p-2">
                                <a href="{{ route('nav.trabalhe') }}">
                                    <div>Trabalhe conosco</div>
                                </a>
                            </li>
                            <li class="d-flex p-2">
                                <a href="{{ route('nav.contato') }}">
                                    <div>Contato</div>
                                </a>
                            </li>

                        </ul>
                    </nav>
                </div>
            </div>
            <div id="header-trigger"><i class="icon-line-menu"></i><i class="icon-line-cross"></i></div>
        </header>


        @yield('content')


        <footer id="footer" class="dark noborder" style="background-color: #080808;">
            <div class="container clearfix">

                <div class="footer-widgets-wrap clearfix" style="padding: 30px;">
                    <div class="row clearfix">
                        <div class="col-12">
                            <div class="col_one_fourth">
                                <div class="widget widget_links clearfix">
                                    <h4>Maschieto</h4>
                                    <ul>
                                        <li><a href="{{ route('nav.sobre') }}">Sobre Nós</a></li>
                                        <li><a href="{{ route('nav.contato') }}">Contato</a></li>
                                        <li><a href="{{ route('nav.trabalhe') }}">Trabalhe Conosco</a></li>
                                        <li><a href="{{ route('nav.revenda') }}">Seja Revendedor</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col_one_fourth">
                                <div class="widget widget_links clearfix">
                                    <h4>Endereço</h4>
                                    <ul>
                                        <li>{{ $informations->address }}, {{ $informations->complement }}</li>
                                        <li>{{ $informations->district }}, {{ $informations->zipcode }}</li>
                                        <li>{{ $informations->mailbox }}</li>
                                        <li>{{ $informations->city }}/{{ $informations->state }}</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col_one_fourth">
                                <div class="widget widget_links clearfix">
                                    <h4>Contatos</h4>
                                    <ul>

                                        <li style="{{ isset($informations->phone1)!=""?'':'display:none;' }}">
                                            <i class="fas fa-phone-square-alt"></i>
                                            <a href="tel:<?= preg_replace('/[^0-9]+/','', $informations->phone1)?>">{{ $informations->phone1 }}</a>
                                        </li>

                                        <li style="{{ isset($informations->phone2)!=""?'':'display:none;' }}">
                                            <i class="fas fa-phone-square-alt"></i>
                                            <a href="tel:<?= preg_replace('/[^0-9]+/','', $informations->phone2)?>">{{ $informations->phone2 }}</a>
                                        </li>

                                        <li style="{{ isset($informations->phone3)!=""?'':'display:none;' }}">
                                            <i class="fas fa-phone-square-alt"></i>
                                            <a href="tel:<?= preg_replace('/[^0-9]+/','', $informations->phone3)?>">{{ $informations->phone2 }}</a>
                                        </li>

                                        {{-- <li style="{{ isset($informations->email1)!=""?'':'display:none;' }}">
                                            <i class="fas fa-at"></i>
                                            <a href="mailto:{{ $informations->email1 }}">{{ $informations->email1 }}</a>
                                        </li> --}}

                                        <li style="{{ isset($informations->email2)!=""?'':'display:none;' }}">
                                            <i class="fas fa-at"></i>
                                            <a href="mailto:{{ $informations->email2 }}">{{ $informations->email2 }}</a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="col_one_fourth col_last">
                                <div class="widget widget_links clearfix">
                                    <h4>Linhas de Produtos</h4>
                                    <div class="row">
                                        <div class="col-12">
                                            <ul>
                                                @foreach ($categories as $category)
                                                    <li><a href="{{ route('nav.produtos') }}">{{ $category->title }}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 fright tright col_last">
                            <img src="{{ asset('images/logo-footer.png') }}" alt="" height="50">
                            <br>
                            <div class="text-center mt-5" style="color: #555">
                                <span>&copy; Maschieto {{ now()->year }}. Todos os direitos reservados.</span>
                                <div class="clear"></div>
                                <p style="margin-top: 10px;margin-bottom:0px!important;">Desenvolvido por: <a
                                        href="http://leddigital.com.br" target="_blank">LED Digital</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <div id="gotoTop" class="icon-angle-up"></div>



    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/plugins.js') }}"></script>
    <script src="{{ asset('js/360rotator.js') }}"></script>

    <script src="{{ asset('js/components/bs-select.js') }}"></script>

    <script src="{{ asset('js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script src="{{ asset('js/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script src="{{ asset('js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{ asset('js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ asset('js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script src="{{ asset('js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ asset('js/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script src="{{ asset('js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ asset('js/extensions/revolution.extension.video.min.js') }}"></script>

    <script src="{{ asset('js/jquery.magnific-popup.min.js')}}"></script>

    <!-- contact js -->
    <script src="{{ asset('js/jquery.form.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/contact.js') }}"></script>


    <script src="{{ asset('js/functions.js') }}"></script>
    <script src="{{ asset('js/lightbox.js') }}"></script>

    {{-- <script>
        //Car Appear In View
        function isScrolledIntoView(elem) {
            var docViewTop = $(window).scrollTop();
            var docViewBottom = docViewTop + $(window).height();

            var elemTop = $(elem).offset().top + 180;
            var elemBottom = elemTop + $(elem).height() - 500;

            return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
        }

        $(window).scroll(function () {
            $('.running-car').each(function () {
                if (isScrolledIntoView(this) === true) {
                    $(this).addClass('in-view');
                } else {
                    $(this).removeClass('in-view');
                }
            });
        });

        //threesixty degree
        window.onload = init;
        var car;

        function init() {

            car = $('.360-car').ThreeSixty({
                totalFrames: 52, // Total no. of image you have for 360 slider
                endFrame: 52, // end frame for the auto spin animation
                currentFrame: 3, // This the start frame for auto spin
                imgList: '.threesixty_images', // selector for image list
                progress: '.spinner', // selector to show the loading progress
                imagePath: 'images/360degree-cars/', // path of the image assets
                filePrefix: '', // file prefix if any
                ext: '.png', // extention for the assets
                height: 887,
                width: 500,
                navigation: true,
                responsive: true,
            });
        };

        // Video on play on hover
        jQuery(document).ready(function ($) {
            $('.videoplay-on-hover').hover(function () {
                if ($(this).find('video').length > 0) {
                    $(this).find('video').get(0).play();
                }
            }, function () {
                if ($(this).find('video').length > 0) {
                    $(this).find('video').get(0).pause();
                }
            });
        });

    </script> --}}

</body>

</html>


</body>

</html>
