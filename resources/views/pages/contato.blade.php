@extends('layouts.default')
@section('content')

{{-- <section id="page-title" class="page-title-dark" style="padding: 160px 0;">
    <div class="container clearfix" style="z-index: 9;">
        <h1>Contato</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="car-contact.html#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Contato</li>
        </ol>
    </div>
    <div id="page-title-gmap" class="gmap-bg gmap"
        style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></div>
    <div class="video-wrap" style="position: absolute; top: 0; left: 0; height: 100%;">
        <div class="video-overlay" style="background-color: rgba(0,0,0,0.6);"></div>
    </div>
</section> --}}

<section id="page-title" class="page-title-parallax page-title-dark"
    style="background-image: url('{{ asset('mainbanner/'.$informations->main_banner) }}'); background-size: cover; padding: 120px 0;"
    data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
    <div class="container clearfix">
        <h1>Contato</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('nav.index') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Contato</li>
        </ol>
    </div>
</section>

<section id="content">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="heading-block">
                        <h2>{{ $contato->title }}</h2>
                        <p>{{ $contato->short_description }}</p>
                    </div>
                    <div class="form-widget">
                        <div class="form-result"></div>
                        <form class="nobottommargin" id="contactForm" name="contactForm"
                            action="{{ route('send.mail') }}" method="post">
                            @csrf
                            <div class="form-process"></div>
                            <div class="col_one_third">
                                <label for="name">Nome <small>*</small></label>
                                <input type="text" id="name" name="name" value=""
                                    class="sm-form-control border-form-control required" />
                            </div>
                            <div class="col_one_third">
                                <label for="email">E-mail <small>*</small></label>
                                <input type="email" id="email" name="email" value=""
                                    class="required email sm-form-control border-form-control" />
                            </div>
                            <div class="col_one_third col_last">
                                <label for="phone">Telefone</label>
                                <input type="text" id="phone" name="phone" value=""
                                    class="sm-form-control border-form-control" />
                            </div>
                            <div class="clear"></div>
                            <div class="col_two_third">
                                <label for="subjectMail">Assunto <small>*</small></label>
                                <input type="text" id="subjectMail" name="subjectMail" value=""
                                    class="required sm-form-control border-form-control" />
                            </div>
                            <div class="col_one_third col_last">
                                <label for="service">Motivo do contato</label>
                                <select id="service" name="service"
                                    class="sm-form-control custom-select border-form-control">
                                    <option value="">-- Selecione um --</option>
                                    <option value="Dúvida">Dúvida</option>
                                    <option value="Crítica">Crítica</option>
                                    <option value="Sugestão">Sugestão</option>
                                    <option value="Outro">Outro</option>
                                </select>
                            </div>
                            <div class="clear"></div>
                            <div class="col_full">
                                <label for="bodyMessage">Mensagem <small>*</small></label>
                                <textarea class="required sm-form-control border-form-control" id="bodyMessage"
                                    name="bodyMessage" rows="5" cols="30"></textarea>
                            </div>

                            <input type="hidden" name="typeContact" value="contato">

                            <div class="col_full">
                                <button class="button button-3d nomargin" type="submit" id="submit" name="submit"
                                    value="submit">Enviar mensagem</button>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mt-4">
                        <img class="card-img-top" src="images/call.jpg" alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">
                                <div class="mb-3">
                                    <b><i class="fas fa-map-marked-alt"></i>&nbsp;&nbsp;Endereço</b> <br />
                                    {{ $informations->address }}, {{ $informations->complement }} <br />
                                    {{ $informations->district }}, {{ $informations->zipcode }} <br />
                                    {{ $informations->mailbox }} <br />
                                    {{ $informations->city }}/{{ $informations->state }}
                                </div>
                                <div class="mb-3">
                                    <b><i class="fas fa-phone-square-alt"></i>&nbsp;&nbsp;Telefones</b> <br />

                                    <a {{ isset($informations->phone1)!=""?"":"display:none;" }}
                                        href="tel:<?= preg_replace('/[^0-9]+/','', $informations->phone1)?>">
                                        {{ $informations->phone1 }}
                                    </a><br />

                                    <a {{ isset($informations->phone2)!=""?"":"display:none;" }}
                                        href="tel:<?= preg_replace('/[^0-9]+/','', $informations->phone2)?>">
                                        {{ $informations->phone2 }}
                                    </a> <br />

                                    <a {{ isset($informations->phone3)!=""?"":"display:none;" }}
                                        href="tel:<?= preg_replace('/[^0-9]+/','', $informations->phone3)?>">
                                        {{ $informations->phone3 }}
                                    </a>

                                </div>
                                {{-- <div class="mb-3">
                                    <b><i class="fas fa-at"></i>&nbsp;&nbsp;E-mail</b> <br />
                                    <a class="btn-link"
                                        href="mailto:contato@maschieto.com.br"><span>contato@maschieto.com.br</span></a>
                                    <a class="btn-link"
                                        href="mailto:sac@maschieto.com.br"><span>sac@maschieto.com.br</span></a>
                                </div> --}}

                                <div class="mb-3">
                                    <b><i class="far fa-clock"></i>&nbsp;&nbsp;Horário de funcionamento</b> <br />
                                    <p>{{ $informations->opening }}</p>
                                </div>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mt-5">
                            <ul class="list-inline" style="text-align: center;">
                                <li class="list-inline-item">
                                    <a style="{{ isset($informations->facebook)!=""?"":"display:none;" }}"
                                        href="{{ $informations->facebook }}" target="_blank">
                                        <img style="border-radius: 5px;" src="{{ asset('images/midias/facebook.png') }}"
                                            width="32px" alt="facebook">
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a style="{{ isset($informations->instagram)!=""?"":"display:none;" }}"
                                        href="{{ $informations->instagram }}" target="_blank">
                                        <img style="border-radius: 5px;"
                                            src="{{ asset('images/midias/instagram.png') }}" width="32px"
                                            alt="instagram">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!--================Contact Success and Error message Area =================-->
<div id="success" class="modal modal-message fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Mensagem enviada com sucesso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Sua mensagem foi enviada com sucesso. Aguarde nosso contato pelo e-mail ou telefone informados.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modals error -->

<div id="error" class="modal modal-message fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Sua mensagem não pode ser enviada</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Estamos enfrentando problemas técnicos.
                    Por favor tente novamente mais tarde ou contate-nos pelos telefones</p> <br>
                    <ul class="mt-2">
                        <li>{{ $informations->phone1 }}</li>
                        <li>{{ $informations->phone2 }}</li>
                        <li>{{ $informations->phone3 }}</li>
                    </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!--================End Contact Success and Error message Area =================-->


@endsection
