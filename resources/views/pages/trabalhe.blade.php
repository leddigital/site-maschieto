@extends('layouts.default')
@section('content')


<section id="content">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="heading-block">
                        <h2>{{ $trabalhe->title }}</h2>
                        <p>{{ $trabalhe->short_description }}</p>
                    </div>

                    <p class="mt-5">Mande seu currículo para
                        <a href="mailto:{{ $trabalhe->email_contato }}?subject={{ $trabalhe->email_assunto }}" target="_blank">{{ $trabalhe->email_contato }}</a>
                        com o assunto "{{ $trabalhe->email_assunto }}" </p>


                    {{-- <h4 class="mb-3">Perguntas frequentes</h4>
                    <div class="accordion mb-5 accordion-border clearfix">
                        <div class="acctitle"><i class="acc-closed icon-question-sign"></i><i
                                class="acc-open icon-question-sign"></i>Pergunta 1</div>
                        <div class="acc_content clearfix">Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Assumenda, dolorum, vero ipsum molestiae minima odio quo voluptate illum
                            excepturi quam cum voluptates doloribus quae nisi tempore necessitatibus dolores
                            ducimus enim libero eaque explicabo suscipit animi at quaerat aliquid ex expedita
                            perspiciatis? Saepe, aperiam, nam unde quas beatae vero vitae nulla.</div>
                        <div class="acctitle"><i class="acc-closed icon-comments-alt"></i><i
                                class="acc-open icon-comments-alt"></i>Pergunta 2</div>
                        <div class="acc_content clearfix">Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Explicabo, placeat, architecto rem dolorem dignissimos repellat veritatis in
                            et eos doloribus magnam aliquam ipsa alias assumenda officiis quasi sapiente
                            suscipit veniam odio voluptatum. Enim at asperiores quod velit minima officia
                            accusamus cumque eligendi consequuntur fuga? Maiores, quasi, voluptates,
                            exercitationem fuga voluptatibus a repudiandae expedita omnis molestiae alias
                            repellat perferendis dolores dolor.</div>
                        <div class="acctitle"><i class="acc-closed icon-lock3"></i><i
                                class="acc-open icon-lock3"></i>Pergunta 3</div>
                        <div class="acc_content clearfix">Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Possimus, fugiat iste nisi tempore nesciunt nemo fuga? Nesciunt, delectus
                            laboriosam nisi repudiandae nam fuga saepe animi recusandae. Asperiores, provident,
                            esse, doloremque, adipisci eaque alias dolore molestias assumenda quasi saepe nisi
                            ab illo ex nesciunt nobis laboriosam iusto quia nulla ad voluptatibus iste beatae
                            voluptas corrupti facilis accusamus recusandae sequi debitis reprehenderit
                            quibusdam. Facilis eligendi a exercitationem nisi et placeat excepturi velit!</div>
                        <div class="acctitle"><i class="acc-closed icon-credit"></i><i
                                class="acc-open icon-credit"></i>Pergunta 4
                        </div>
                        <div class="acc_content clearfix">Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Possimus, fugiat iste nisi tempore nesciunt nemo fuga? Nesciunt, delectus
                            laboriosam nisi repudiandae nam fuga saepe animi recusandae. Asperiores, provident,
                            esse, doloremque, adipisci eaque alias dolore molestias assumenda quasi saepe nisi
                            ab illo ex nesciunt nobis laboriosam iusto quia nulla ad voluptatibus iste beatae
                            voluptas corrupti facilis accusamus recusandae sequi debitis reprehenderit
                            quibusdam. Facilis eligendi a exercitationem nisi et placeat excepturi velit!</div>
                    </div> --}}

                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-md-6 col-lg-12">
                            <div class="card bg-dark dark mb-5 pb-2 px-2">
                                <div class="card-body">
                                    <h3 class="mb-3 uppercase ls1">Conheça a Maschieto</h3>
                                    <p class="card-text text-white-50">Nossa indústria foi fundada em 1968 com o propósito de realizar
                                        o desejo das pessoas em encontrar móveis que completam ambientes com estilo, design e conforto.</p>
                                    <a href="{{ route('nav.sobre') }}" class="button button-3d button-rounded m-0">saiba mais</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>


@endsection
