@extends('layouts.default')
@section('content')


<section id="page-title" class="page-title-parallax page-title-dark"
    style="background-image: url('{{ asset('mainbanner/'.$informations->main_banner) }}'); background-size: cover; padding: 140px 0;"
    data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
    <div class="container clearfix">
        <h1>A Maschieto</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('nav.index') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="#">A Maschieto</a></li>
        </ol>
    </div>
</section>

<section id="content">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="heading-block">
                        <h2>{{ $sobre->title }}</h2>
                            {!! $sobre->content !!}
                    </div>

                    <div class="text-about my-5">
                        {{-- <p>Praesent sed maximus risus, et placerat eros. Donec at tincidunt velit, consequat dictum
                            ipsum. Aliquam a tellus diam. Nulla arcu ex,
                            dignissim nec ligula facilisis, suscipit aliquam nisi. Nulla nec varius nibh. Quisque quis
                            justo a tortor fringilla hendrerit ut non
                            est. Donec quis nibh et lorem hendrerit suscipit eget et nulla. Suspendisse pulvinar augue
                            ut justo fermentum, vitae placerat ligula
                            volutpat. Nunc id dapibus eros, a mollis tellus. Nunc semper venenatis euismod. Etiam vel
                            mattis nunc, eu tempus magna. Integer sagittis
                            lorem id libero porta, eget aliquet lorem tincidunt. Proin fermentum elit sit amet lorem
                            egestas dapibus. Praesent ac semper lectus.
                            Integer interdum malesuada odio, a iaculis justo placerat eu.</p>

                        <div class="jumbotron p-4 p-md-5 dark position-relative rounded"
                            style="background: url('images/hero-slider/2.jpg') center center / cover;">
                            <div class="col-md-6 px-0 py-3" style="z-index: 1">
                                <h2 class="t600 mb-2">Nulla auctor quam nec magna luctus finibus.</h2>
                                <p class="my-4 h6 text-white-50">Nunc semper venenatis euismod. Etiam vel mattis nunc,
                                    eu tempus magna.
                                    Ut leo orci, malesuada id pellentesque a, viverra quis orci.
                                </p>
                            </div>
                            <div class="video-wrap rounded"
                                style="position: absolute; top: 0; left: 0; height: 100%; z-index: 0">
                                <div class="video-overlay" style="background: rgba(0,0,0,0.5);"></div>
                            </div>
                        </div> --}}

                        {{-- Vídeo Frame --}}
                        {!! $sobre->video !!}


                    </div>

                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-md-6 col-lg-12">
                            <div class="card bg-dark dark mb-5 pb-2 px-2">
                                <div class="card-body">
                                    <h3 class="mb-3 uppercase ls1">{{ $chamadaRevenda->title }}</h3>
                                    <p class="card-text text-white-50">{{ $chamadaRevenda->short_description }}</p>
                                    <a href="{{ route('nav.revenda') }}" class="button button-3d button-rounded m-0">{{ $chamadaRevenda->content }}</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-12">
                            <div class="card mb-5 shadow-sm">
                                <div class="card-body">
                                    <h3 class="card-title">{{ $chamadaTrabalhe->title }}</h3>
                                    <p>{{ $chamadaTrabalhe->short_description }}</p>
                                    <a href="{{ route('nav.trabalhe') }}" class="button button-3d button-rounded m-0">{{ $chamadaTrabalhe->content }}</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-12">
                            <div class="card">
                                <img class="card-img-top" src="images/call.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <h4 class="mb-1 color">{{ $chamadaContato->title }}</h4>
                                    <p class="card-text mb-4">{{ $chamadaContato->short_description }}</p>
                                    <a href="{{ route('nav.contato') }}" class="button button-3d button-rounded m-0">{{ $chamadaContato->content }}</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</section>



<!--================Contact Success and Error message Area =================-->
<div id="success" class="modal modal-message fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></span>
                    <h2 class="modal-title">Agradecemos seu contato</h2>
                    <p class="modal-subtitle">Sua mensagem foi recebida. Entraremos em contato com você em breve pelo telefone ou e-mail informados</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Modals error -->

    <div id="error" class="modal modal-message fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></span>
                    <h2 class="modal-title">Ops, algo deu errado</h2>
                    <p class="modal-subtitle"> Por favor tente novamente mais tarde </p>
                </div>
            </div>
        </div>
    </div>
    <!--================End Contact Success and Error message Area =================-->


@endsection
