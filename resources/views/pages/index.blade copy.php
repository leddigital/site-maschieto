@extends('layouts.default')
@section('content')

<section id="slider" class="slider-element swiper_wrapper full-screen force-full-screen clearfix" data-dots="true"
    data-loop="true" data-grab="false">
    <div class="swiper-container swiper-parent">
        <div class="swiper-wrapper">


            @foreach ($banners as $banner)
                @if ($banner->position == "header")
                    <div class="swiper-slide dark"
                        style="background-image: url('{{ asset('banners/'.$banner->image) }}'); background-size: cover">
                        <div class="container clearfix">
							<div class="slider-caption {{ $banner->textalign }}">
                            <h2 style="color:{{ $banner->color }}" class="font-primary nott">{{ $banner->info1 }}</h2>
								<p style="color:{{ $banner->color }}" class="t300 font-primary d-none d-sm-block">{{ $banner->info2 }}</p>

                                @if (!empty($banner->link1))
                                <a style="color:{{ $banner->color }};border-color:{{ $banner->color }}"
                                    href="{{ $banner->link1 }}"
                                        class="button button-rounded button-border button-light nott">Saiba mais</a>
                                @endif

							</div>
						</div>
                    </div>
                @endif
            @endforeach



        </div>
        <div class="swiper-pagination"></div>
    </div>
</section>

<section id="content" class="clearfix">
    <div class="content-wrap nobottompadding" style="padding-top: 1px">

        <div class="section notopmargin clearfix" style="padding-top: 100px; margin-bottom:0px!important;">


            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-6">

                        <img src="{{ asset('content/'.$sobre->id.'/'.$sobre->image) }}" alt="{{ $sobre->title }}">

                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="container clearfix">
                            <div class="row clearfix" style="position: relative;">

                                <div class="heading-block hlarge">
                                    <h3>{{ $sobre->title }}</h3>
                                </div>
                                {{ $sobre->short_description }}
                                    <a class="mt-3" href="{{ route('nav.sobre') }}">
                                        <button class="tp-caption button button-black button-dark button-circle button-large nott tp-resizeme">
                                            Saiba mais
                                        </button>
                                    </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section product-slider-pers nobottommargin clearfix"
            style="background: #FFF url('images/bg.jpg') center 65% no-repeat; background-size: 100% auto; padding: 100px 0 10px">
            <div class="container clearfix">
                <div id="rev_slider_424_1_wrapper"
                    class="rev_slider_wrapper nomargin nopadding fullwidthbanner-container" data-alias="image-gallery">

                    <div id="rev_slider_424_1" class="rev_slider fullwidthabanner" style="display:none;color:#080808;"
                        data-version="5.2.0">
                        <ul>

                            <li data-index="rs-1479" data-transition="slidefromleft" data-slotamount="default"
                                data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default"
                                data-easeout="default" data-masterspeed="300" data-thumb="images/cat-sofa-200.png"
                                data-rotate="0" data-saveperformance="off" data-title="Sofás" data-param1="Linha"
                                data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
                                data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                            <img src="{{ asset('demos/assets/images/dummy.png') }}" alt=""
                                    data-lazyload="images/cat-sofa.png"
                                    data-bgposition="left center" data-bgfit="contain" data-bgrepeat="no-repeat"
                                    class="rev-slidebg" data-no-retina>

                                <div class="tp-caption font-body ls2 uppercase tp-resizeme" id="slide-1479-layer-1"
                                    data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']"
                                    data-y="['top','top','top','top']" data-voffset="['0','0','0','90']"
                                    data-fontsize="['15','15','13','13']" data-lineheight="['15','15','13','13']"
                                    data-width="['370','370','290','210']" data-height="none" data-whitespace="nowrap"
                                    data-type="text" data-actions='' data-basealign="slide" data-responsive_offset="on"
                                    data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":400,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-start="600" data-splitin="none" data-splitout="none"
                                    data-responsive_offset="on"
                                    style="z-index: 6; min-width: 370px; max-width: 370px; color: #333; white-space: nowrap;">
                                    </div>

                                <div class="tp-caption font-primary uppercase t700 tp-resizeme" id="slide-1479-layer-2"
                                    data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']"
                                    data-y="['top','top','top','top']" data-voffset="['30','30','92','68']"
                                    data-fontsize="['40','40','30','20']" data-lineheight="['40','40','30','20']"
                                    data-width="['500','500','400','210']" data-height="none" data-whitespace="normal"
                                    data-type="text" data-actions='' data-basealign="slide" data-responsive_offset="on"
                                    data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":600,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                    style="z-index: 7; color: #333; letter-spacing: 2px; white-space: normal;">
                                    Sofás</div>

                                <div class="tp-caption font-body ls0 tp-resizeme" id="slide-1479-layer-4"
                                    data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']"
                                    data-y="['top','top','top','top']" data-voffset="['200','200','215','154']"
                                    data-fontsize="['14','14','14','13']" data-lineheight="['23','23','21','20']"
                                    data-width="['360','360','290','210']" data-height="none" data-whitespace="normal"
                                    data-type="text" data-actions='' data-basealign="slide" data-responsive_offset="on"
                                    data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                    style="z-index: 8; min-width: 360px; max-width: 360px; color: #333; white-space: normal;">
                                    Conforto, design e qualidade. Nossos sofás renovam o ambiente da casa e valorizam a decoração.</div>

                                <div class="tp-caption button button-black button-dark button-circle button-large nott tp-resizeme"
                                    id="slide-1479-layer-5" data-x="['right','right','right','right']"
                                    data-hoffset="['25','25','25','25']" data-y="['top','top','top','top']"
                                    data-voffset="['336','346','336','366']" data-width="none" data-height="none"
                                    data-whitespace="nowrap" data-type="text" data-actions='' data-basealign="slide"
                                    data-responsive_offset="on" data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":1200,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                    style="z-index: 9; white-space: nowrap;cursor:pointer;">Saiba Mais
                                </div>
                            </li>

                            <li data-index="rs-1480" data-transition="slidefromleft" data-slotamount="default"
                                data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default"
                                data-easeout="default" data-masterspeed="300" data-thumb="images/cat-poltrona-200.png"
                                data-rotate="0" data-saveperformance="off" data-title="Poltronas" data-param1="Linha"
                                data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
                                data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                            <img src="{{ asset('demos/assets/images/dummy.png') }}" alt=""
                                    data-lazyload="images/cat-poltrona.png"
                                    data-bgposition="left center" data-bgfit="contain" data-bgrepeat="no-repeat"
                                    class="rev-slidebg" data-no-retina>

                                <div class="tp-caption font-body ls2 uppercase tp-resizeme" id="slide-1480-layer-1"
                                    data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']"
                                    data-y="['top','top','top','top']" data-voffset="['0','0','0','90']"
                                    data-fontsize="['15','15','13','13']" data-lineheight="['15','15','13','13']"
                                    data-width="['370','370','290','210']" data-height="none" data-whitespace="nowrap"
                                    data-type="text" data-actions='' data-basealign="slide" data-responsive_offset="on"
                                    data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":400,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-start="600" data-splitin="none" data-splitout="none"
                                    data-responsive_offset="on"
                                    style="z-index: 6; min-width: 370px; max-width: 370px; color: #333; white-space: nowrap;">
                                    </div>

                                <div class="tp-caption font-primary uppercase t700 tp-resizeme" id="slide-1480-layer-2"
                                    data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']"
                                    data-y="['top','top','top','top']" data-voffset="['30','30','92','68']"
                                    data-fontsize="['40','40','30','20']" data-lineheight="['40','40','30','20']"
                                    data-width="['500','500','400','210']" data-height="none" data-whitespace="normal"
                                    data-type="text" data-actions='' data-basealign="slide" data-responsive_offset="on"
                                    data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":600,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                    style="z-index: 7; color: #333; letter-spacing: 2px; white-space: normal;">
                                    Poltronas</div>

                                <div class="tp-caption font-body ls0 tp-resizeme" id="slide-1480-layer-4"
                                    data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']"
                                    data-y="['top','top','top','top']" data-voffset="['200','200','215','154']"
                                    data-fontsize="['14','14','14','13']" data-lineheight="['23','23','21','20']"
                                    data-width="['360','360','290','210']" data-height="none" data-whitespace="normal"
                                    data-type="text" data-actions='' data-basealign="slide" data-responsive_offset="on"
                                    data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                    style="z-index: 8; min-width: 360px; max-width: 360px; color: #333; white-space: normal;">
                                    Ponto diferencial de ambientes, nossas poltronas oferecem conforto para quem usa e encanta quem vê.</div>

                                <div class="tp-caption button button-black button-dark button-circle button-large nott tp-resizeme"
                                    id="slide-1480-layer-5" data-x="['right','right','right','right']"
                                    data-hoffset="['25','25','25','25']" data-y="['top','top','top','top']"
                                    data-voffset="['336','346','336','366']" data-width="none" data-height="none"
                                    data-whitespace="nowrap" data-type="text" data-actions='' data-basealign="slide"
                                    data-responsive_offset="on" data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":1200,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                    style="z-index: 9; white-space: nowrap;cursor:pointer;">Saiba Mais
                                </div>
                            </li>
                            <li data-index="rs-1481" data-transition="slidefromleft" data-slotamount="default"
                                data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default"
                                data-easeout="default" data-masterspeed="300" data-thumb="images/cat-cadeira-200.png"
                                data-rotate="0" data-saveperformance="off" data-title="Cadeiras"
                                data-param1="Linha" data-param2="" data-param3="" data-param4="" data-param5=""
                                data-param6="" data-param7="" data-param8="" data-param9="" data-param10=""
                                data-description="">

                            <img src="{{ asset('demos/assets/images/dummy.png') }}" alt=""
                                    data-lazyload="images/cat-cadeira.png"
                                    data-bgposition="left center" data-bgfit="contain" data-bgrepeat="no-repeat"
                                    class="rev-slidebg" data-no-retina>

                                <div class="tp-caption font-body ls2 uppercase tp-resizeme" id="slide-1481-layer-1"
                                    data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']"
                                    data-y="['top','top','top','top']" data-voffset="['0','0','0','90']"
                                    data-fontsize="['15','15','13','13']" data-lineheight="['15','15','13','13']"
                                    data-width="['370','370','290','210']" data-height="none" data-whitespace="nowrap"
                                    data-type="text" data-actions='' data-basealign="slide" data-responsive_offset="on"
                                    data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":400,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-start="600" data-splitin="none" data-splitout="none"
                                    data-responsive_offset="on"
                                    style="z-index: 6; min-width: 370px; max-width: 370px; color: #333; white-space: nowrap;">
                                    </div>

                                <div class="tp-caption font-primary uppercase t700 tp-resizeme" id="slide-1481-layer-2"
                                    data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']"
                                    data-y="['top','top','top','top']" data-voffset="['30','30','92','68']"
                                    data-fontsize="['40','40','30','20']" data-lineheight="['40','40','30','20']"
                                    data-width="['500','500','400','210']" data-height="none" data-whitespace="normal"
                                    data-type="text" data-actions='' data-basealign="slide" data-responsive_offset="on"
                                    data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":600,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                    style="z-index: 7; color: #333; letter-spacing: 2px; white-space: normal;">
                                    Cadeiras</div>

                                <div class="tp-caption font-body ls0 tp-resizeme" id="slide-1481-layer-4"
                                    data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']"
                                    data-y="['top','top','top','top']" data-voffset="['200','200','215','154']"
                                    data-fontsize="['14','14','14','13']" data-lineheight="['23','23','21','20']"
                                    data-width="['360','360','290','210']" data-height="none" data-whitespace="normal"
                                    data-type="text" data-actions='' data-basealign="slide" data-responsive_offset="on"
                                    data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                    style="z-index: 8; min-width: 360px; max-width: 360px; color: #333; white-space: normal;">
                                    Essenciais para a composição de ambientes. Nosso portfólio apresenta ampla variedade de modelos para você escolher.</div>

                                <div class="tp-caption button button-black button-dark button-circle button-large nott tp-resizeme"
                                    id="slide-1481-layer-5" data-x="['right','right','right','right']"
                                    data-hoffset="['25','25','25','25']" data-y="['top','top','top','top']"
                                    data-voffset="['336','346','336','366']" data-width="none" data-height="none"
                                    data-whitespace="nowrap" data-type="text" data-actions='' data-basealign="slide"
                                    data-responsive_offset="on" data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":1200,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                    style="z-index: 9; white-space: nowrap;cursor:pointer;">Saiba Mais
                                </div>
                            </li>

                            <li data-index="rs-1482" data-transition="slidefromleft" data-slotamount="default"
                                data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default"
                                data-easeout="default" data-masterspeed="300" data-thumb="images/cat-aparador-200.png"
                                data-rotate="0" data-saveperformance="off" data-title="Aparadores" data-param1="Linha"
                                data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
                                data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                            <img src="{{ asset('demos/assets/images/dummy.png') }}" alt="" data-lazyload="images/cat-aparador.png"
                                    data-bgposition="left center" data-bgfit="contain" data-bgrepeat="no-repeat"
                                    class="rev-slidebg" data-no-retina>

                                <div class="tp-caption font-body ls2 uppercase tp-resizeme" id="slide-1482-layer-1"
                                    data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']"
                                    data-y="['top','top','top','top']" data-voffset="['40','40','40','90']"
                                    data-fontsize="['15','15','13','13']" data-lineheight="['15','15','13','13']"
                                    data-width="['370','370','290','210']" data-height="none" data-whitespace="nowrap"
                                    data-type="text" data-actions='' data-basealign="slide" data-responsive_offset="on"
                                    data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":400,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-start="600" data-splitin="none" data-splitout="none"
                                    data-responsive_offset="on"
                                    style="z-index: 6; min-width: 370px; max-width: 370px; color: #333; white-space: nowrap;">
                                    </div>

                                <div class="tp-caption font-primary uppercase t700 tp-resizeme" id="slide-1482-layer-2"
                                    data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']"
                                    data-y="['top','top','top','top']" data-voffset="['70','70','92','68']"
                                    data-fontsize="['40','40','30','20']" data-lineheight="['40','40','30','20']"
                                    data-width="['500','500','400','210']" data-height="none" data-whitespace="normal"
                                    data-type="text" data-actions='' data-basealign="slide" data-responsive_offset="on"
                                    data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":600,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                    style="z-index: 7; color: #333; letter-spacing: 2px; white-space: normal;">
                                    Aparadores</div>

                                <div class="tp-caption font-body ls0 tp-resizeme" id="slide-1482-layer-4"
                                    data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']"
                                    data-y="['top','top','top','top']" data-voffset="['200','200','215','154']"
                                    data-fontsize="['14','14','14','13']" data-lineheight="['23','23','21','20']"
                                    data-width="['360','360','290','210']" data-height="none" data-whitespace="normal"
                                    data-type="text" data-actions='' data-basealign="slide" data-responsive_offset="on"
                                    data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                    style="z-index: 8; min-width: 360px; max-width: 360px; color: #333; white-space: normal;">
                                    Multifuncionais, podem servir de guarda-objetos. Funcionais para decoração, trazem modernidade para o ambiente. Seja qual for a finalidade de uso, nossos aparadores contam com exclusivo design e estilo, em várias opções de acabamento, o que faz com que ele possa ser usado em qualquer cômodo da casa.</div>

                                <div class="tp-caption button button-black button-dark button-circle button-large nott tp-resizeme"
                                    id="slide-1482-layer-5" data-x="['right','right','right','right']"
                                    data-hoffset="['25','25','25','25']" data-y="['top','top','top','top']"
                                    data-voffset="['336','346','336','366']" data-width="none" data-height="none"
                                    data-whitespace="nowrap" data-type="text" data-actions='' data-basealign="slide"
                                    data-responsive_offset="on" data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":1200,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                    style="z-index: 9; white-space: nowrap;cursor:pointer;">Saiba Mais
                                </div>
                            </li>

                            <li data-index="rs-1483" data-transition="slidefromleft" data-slotamount="default"
                                data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default"
                                data-easeout="default" data-masterspeed="300" data-thumb="images/cat-banqueta-200.png"
                                data-rotate="0" data-saveperformance="off" data-title="Banqueta"
                                data-param1="Linha" data-param2="" data-param3="" data-param4="" data-param5=""
                                data-param6="" data-param7="" data-param8="" data-param9="" data-param10=""
                                data-description="">

                            <img src="{{ asset('demos/assets/images/dummy.png') }}" alt="" data-lazyload="images/cat-banqueta.png"
                                    data-bgposition="left center" data-bgfit="contain" data-bgrepeat="no-repeat"
                                    class="rev-slidebg" data-no-retina>

                                <div class="tp-caption font-body ls2 uppercase tp-resizeme" id="slide-1483-layer-1"
                                    data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']"
                                    data-y="['top','top','top','top']" data-voffset="['40','40','40','90']"
                                    data-fontsize="['15','15','13','13']" data-lineheight="['15','15','13','13']"
                                    data-width="['370','370','290','210']" data-height="none" data-whitespace="nowrap"
                                    data-type="text" data-actions='' data-basealign="slide" data-responsive_offset="on"
                                    data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":400,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-start="600" data-splitin="none" data-splitout="none"
                                    data-responsive_offset="on"
                                    style="z-index: 6; min-width: 370px; max-width: 370px; color: #333; white-space: nowrap;">
                                    </div>

                                <div class="tp-caption font-primary uppercase t700 tp-resizeme" id="slide-1483-layer-2"
                                    data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']"
                                    data-y="['top','top','top','top']" data-voffset="['70','70','92','68']"
                                    data-fontsize="['40','40','30','20']" data-lineheight="['40','40','30','20']"
                                    data-width="['500','500','400','210']" data-height="none" data-whitespace="normal"
                                    data-type="text" data-actions='' data-basealign="slide" data-responsive_offset="on"
                                    data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":600,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                    style="z-index: 7; color: #333; letter-spacing: 2px; white-space: normal;">
                                    Banqueta</div>

                                <div class="tp-caption font-body ls0 tp-resizeme" id="slide-1483-layer-4"
                                    data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']"
                                    data-y="['top','top','top','top']" data-voffset="['200','200','215','154']"
                                    data-fontsize="['14','14','14','13']" data-lineheight="['23','23','21','20']"
                                    data-width="['360','360','290','210']" data-height="none" data-whitespace="normal"
                                    data-type="text" data-actions='' data-basealign="slide" data-responsive_offset="on"
                                    data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                    style="z-index: 8; min-width: 360px; max-width: 360px; color: #333; white-space: normal;">
                                    Existem diferentes formatos, materiais e dimensões. O conforto das nossas banquetas é incontestável e elas se adequam aos mais diversos estilos.</div>

                                <div class="tp-caption button button-black button-dark button-circle button-large nott tp-resizeme"
                                    id="slide-1483-layer-5" data-x="['right','right','right','right']"
                                    data-hoffset="['25','25','25','25']" data-y="['top','top','top','top']"
                                    data-voffset="['336','346','336','366']" data-width="none" data-height="none"
                                    data-whitespace="nowrap" data-type="text" data-actions='' data-basealign="slide"
                                    data-responsive_offset="on" data-textAlign="['right','right','right','right']"
                                    data-frames='[{"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":1200,"ease":"Power4.easeOut"},{"delay":"wait","speed":900,"to":"opacity:0;","ease":"nothing"}]'
                                    data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                    style="z-index: 9; white-space: nowrap;cursor:pointer;">Saiba Mais
                                </div>
                            </li>
                        </ul>
                        <div class="tp-bannertimer hidden"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section nomargin nobg clearfix" style="padding: 100px 0;">
            <div class="container clearfix">
                <div class="heading-block center">
                    <div class="before-heading uppercase ls1" style="font-size: 13px; font-style: normal;">Lorem
                        ipsum dolor sit amet, consectetur adipisicing.</div>
                    <h3 class="t700">Produtos em Destaque</h3>
                </div>

                {{-- <ul class="portfolio-filter style-2 clearfix" data-container="#portfolio">
                    <li><a class="link-filter" data-filter=".cf-cuv"><i></i><span>Sofás</span></a></li>
                    <li><a class="link-filter" data-filter=".cf-sedan"><i></i><span>Mesas</span></a></li>
                    <li><a class="link-filter" data-filter=".cf-supercar"><i></i><span>Cadeiras</span></a></li>
                    <li><a class="link-filter" data-filter=".cf-hatchback"><i></i><span>Poltronas</span></a></li>
                    <li><a class="link-filter" data-filter=".cf-cabriolet"><i></i><span>Aparadores</span></a></li>

                    <li class="fright activeFilter"><a class="button button-small button-rounded button-reset"
                            href="javascript:;" data-filter="*">Mostrar Todos</a></li>
                </ul> --}}

                <div class="clear mb-5"></div>

                <div id="portfolio" class="portfolio portfolio-3 grid-container clearfix" data-layout="fitRows">

                    <article class="portfolio-item cf-rack" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/rack-painel_trend.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Rack Trend</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-poltrona" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/pont_anny.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Poltrona Anny</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-sofa" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/sofa_folha_2l.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Sofá Folha 2 lugares</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-banqueta" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/banq_natalia.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Banqueta Natália</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-mesa" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/mesa_lyra.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Mesa Lyra</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-cadeira" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/cad_mirella.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Cadeira Mirella</a></h3>
                        </div>
                    </article>

                </div>
            </div>
        </div>



        <section id="slider" class="slider-element swiper_wrapper full-screen force-full-screen clearfix"
            data-dots="true" data-loop="true" data-grab="true">
            <div class="swiper-container swiper-parent">
                <div class="swiper-wrapper">

                    @foreach ($banners as $banner)
                        @if ($banner->position == "footer")
                            <div class="swiper-slide dark" style="background-image: url('{{ asset('banners/'.$banner->image) }}'); background-size: cover">
                                <div class="container clearfix">
                            </div>
                        </div>
                        @endif
                    @endforeach

                </div>
                <div class="swiper-pagination"></div>
            </div>


        </section>





        <div class="section nomargin nopadding clearfix">
            <div class="row align-items-stretch clearfix">

                <div class="col-lg-6 dark bgcolor"
                style="background: url('{{ asset('images/5.jpg') }}') center center no-repeat; background-size: cover; min-height: 400px">
                    <div class="col-padding clearfix" style="min-height: 400px">
                        <div class="text-wrappper-footer" style="margin: 10%">
                            <div class="heading-block noborder" style="margin-bottom: 20px;">
                                <h3>Trabalhe conosco</h3>
                            </div>
                            <p>Trabalhe na Móveis Maschieto. Se você tem talento e comprometimento no que faz, queremos falar com você.</p>
                            <a href="{{ route('nav.trabalhe') }}"
                                class="button button-rounded button-white button-light nomargin">Saiba mais</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 clearfix bgcolor"
                style="background: url('{{ asset('images/background.jpg') }}') center center no-repeat; background-size: cover; min-height: 400px">
                    <div class="col-padding clearfix" style="min-height: 400px">
                        <div class="text-wrappper-footer" style="margin: 10%">
                            <div class="heading-block noborder" style="margin-bottom: 20px;">
                                <h3 style="color:#fff">Seja uma revenda Maschieto</h3>
                            </div>
                            <p style="color:#FFF">Trabalhe com a qualidade e conceito dos Móveis Maschieto!</p>
                            <a style="margin-top: 2rem !important;" href="{{ route('nav.revenda') }}"
                                class="button button-large button-dark button-black button-rounded nomargin">Entre em
                                contato</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

@endsection
