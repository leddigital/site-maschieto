@extends('layouts.default')
@section('content')

<section id="content" class="clearfix">
    <div class="content-wrap pt-5">
        <div class="container">
            <div class="row">

                <div class="col-md-8">

                    <div id="section-features" class="page-section">

                        <img class="mb-5" src="{{ asset('imgprodutos/'.$produto->image) }}" />

                        <h3 class="mb-3">{{ $produto->title }}</h3>


                        {{-- <p>{!! $produto->content !!}</p> --}}




                        {{-- <div class="row clearfix">
                            <div class="col-md-6">
                                <ul class="iconlist">
                                    <li><i class="icon-car-paint"></i><span class="ml-2">Fabricação Manual</span></li>
                                    <li class="mt-2"><i class="icon-car-stearing"></i><span class="ml-2">Madeira:
                                            Nogueira</span></li>
                                    <li class="mt-2"><i class="icon-car-fuel"></i><span class="ml-2">Estofado:
                                            Visco</span></li>
                                    <li class="mt-2"><i class="icon-car-signal"></i><span class="ml-2">Tecido:
                                            Poliéster</span></li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="iconlist">
                                    <li><i class="icon-car-meter"></i><span class="ml-2">Altura: 90cm</span></li>
                                    <li class="mt-2"><i class="icon-car-wheel"></i><span class="ml-2">Largura:
                                            1,5m</span></li>
                                    <li class="mt-2"><i class="icon-car-signal"></i><span class="ml-2">Largura:
                                            30cm</span></li>
                                    <li class="mt-2"><i class="icon-car-alert"></i><span class="ml-2">Peso: 10kg</span>
                                    </li>
                                </ul>
                            </div>
                        </div> --}}
                    </div>

                    {{-- <div id="section-gallery" class="page-section my-5">
                        <h3 class="mb-3">Galeria de imagens</h3>
                        <div class="fslider flex-thumb-grid grid-6" data-animation="fade" data-speed="900"
                            data-pagi="false" data-thumbs="true">
                            <div class="flexslider">
                                <div class="slider-wrap">
                                    <div class="slide" data-thumb="images/single/1.jpg">
                                        <img src="images/single/1.jpg" alt="Image">
                                        <div class="flex-caption slider-caption-bg slider-caption-bg-light">
                                            Lorem ipsum</div>
                                    </div>
                                    <div class="slide" data-thumb="images/single/2.jpg">
                                        <img src="images/single/2.jpg" alt="Image">
                                        <div class="flex-caption slider-caption-bg slider-caption-bg-light">
                                            Lorem ipsum</div>
                                    </div>
                                    <div class="slide" data-thumb="images/single/3.jpg">
                                        <img src="images/single/3.jpg" alt="Image">
                                        <div class="flex-caption slider-caption-bg slider-caption-bg-light">
                                            Lorem ipsum</div>
                                    </div>
                                    <div class="slide" data-thumb="images/single/4.jpg">
                                        <img src="images/single/4.jpg" alt="Image">
                                        <div class="flex-caption slider-caption-bg slider-caption-bg-light">
                                            Lorem ipsum</div>
                                    </div>
                                    <div class="slide" data-thumb="images/single/5.jpg">
                                        <img src="images/single/5.jpg" alt="Image">
                                        <div class="flex-caption slider-caption-bg slider-caption-bg-light">
                                            Lorem ipsum</div>
                                    </div>
                                    <div class="slide" data-thumb="images/single/6.jpg">
                                        <img src="images/single/6.jpg" alt="Image">
                                        <div class="flex-caption slider-caption-bg slider-caption-bg-light">
                                            Lorem ipsum</div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div> --}}

                    <a href="{{ asset('imgprodutos/descritivos/'.$produto->descritivo) }}"
                        class="button button-3d button-rounded my-3"
                        data-lightbox="image-1"
                        data-title="Manual Aparador Camila">
                        Descritivo técnico
                    </a>

                </div>

                <div class="col-md-4">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h3 class="card-title">Fale conosco</h3>
                            <p>Entre em contato. Envie sua sugestão ou crítica.</p>
                            <a href="{{ route('nav.contato') }}"
                                class="button button-rounded nomargin button-large btn-block">Entre em contato</a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="clear"></div>

            {{-- <div id="section-store" class="page-section mt-3">
                <h3 class="mb-3">Venha conhecer nossa linha completa de produtos.</h3>

                <section id="google-map" class="gmap mb-4"></section>

                <div class="row">
                    <div class="col-md-4">
                        <address>
                            <strong>Móveis Maschieto</strong><br>
                            Rod. Roberto Mário Perosa<br>
                            KM 11.5, Ibirá - SP, 15860-000<br>
                        </address>
                    </div>
                    <div class="col-md-4">
                        <abbr title="Phone Number"><strong>Telefone:</strong></abbr> (17) 3551-1064<br>
                        <abbr title="Email Address"><strong>E-mail:</strong></abbr> <a
                            href="mailto:contato@maschieto.com.br">contato@maschieto.com.br</a>
                    </div>
                </div>
            </div> --}}


        </div>
    </div>
</section>

@endsection
