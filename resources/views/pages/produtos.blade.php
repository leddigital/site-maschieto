@extends('layouts.default')
@section('content')

<section id="page-title" class="page-title-parallax page-title-dark"
    style="background-image: url('{{ asset('mainbanner/'.$informations->main_banner) }}'); background-size: cover; padding: 120px 0;"
    data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
    <div class="container clearfix">
        <h1>Produtos</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('nav.index') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Produtos</li>
        </ol>
    </div>
</section>

<section id="content" class="clearfix" style="overflow: visible">
    <div class="content-wrap notoppadding">
        <div class="container">
            <div class="card p-4 shadow" style="top: -60px;">
                <form action="car-listing.html#" method="post" class="nobottommargin">
                    <div class="row clearfix">
                        <div class="col-md-4 col-sm-6 col-12 mt-4 mt-md-0">
                            <label for="">Nome do produto</label>
                            <input class="form-control" placeholder="Digite para pesquisar..."></input>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12 mt-4 mt-md-0">
                            <label for="">Selecione a linha</label>
                            <select class="selectpicker form-control customjs" title="Linha" data-size="10"
                                data-live-search="true" multiple data-live-search="true" style="width:100%;">
                                <optgroup label="Linhas">

                                    @foreach ($categories as $category)
                                        <option value="{{ $category->url }}">{{ $category->title }}</option>
                                    @endforeach

                                </optgroup>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12 mt-4 mt-md-0">
                            <label for="">Selecione o Produto</label>
                            <select class="selectpicker customjs form-control" data-size="10" data-live-search="true"
                                title="Produto" style="width:100%; line-height: 30px;">
                                <optgroup label="Produto">
                                    <option value="R8">Camila</option>
                                    <option value="TT">Oskar</option>
                                    <option value="S5">Folha</option>
                                    <option value="A5">Asti</option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-6 col-6">
                            <button class="button button-3d button-rounded btn-block noleftmargin"
                                style="margin-top: 29px;">Procurar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="section nomargin pt-0 nobg">
            <div class="container clearfix">

                <ul class="portfolio-filter style-2 clearfix" data-container="#portfolio">
                    @foreach ($categories as $category)
                        <li>
                            <a href="#" data-filter=".{{ $category->url }}">
                                <span>{{ $category->title }}</span>
                            </a>
                        </li>
                    @endforeach
                    <li class="fright activeFilter">
                        <a class="button button-small button-rounded button-reset" href="car-listing.html#" data-filter="*">Mostrar Todos</a>
                    </li>
                </ul>
                <div class="clear"></div>

                <div id="portfolio" class="portfolio portfolio-3 grid-container clearfix" data-layout="fitRows">

                    @foreach ($productList as $category)
                        @foreach ($category->contents as $produto)
                        <article class="portfolio-item {{ $category->url }}" style="padding-bottom: 40px">
                            <div class="portfolio-image">
                                <a href="{{ route('nav.produto', ['url' => $produto->url] ) }}">
                                    <img src="{{ asset('imgprodutos/'.$produto->image) }}" alt="{{ $produto->title }}">
                                </a>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="{{ route('nav.produto', ['url' => $produto->url] ) }}">{{ $produto->title }}</a></h3>
                            </div>
                        </article>
                        @endforeach
                    @endforeach

                </div>
            </div>
        </div>


        <div class="section nomargin footer-stick clearfix dark"
            style="background: url('images/background.jpg') left no-repeat; background-size: cover; padding: 120px 0">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="h2 t700 mb-4" style="color: #fff">Trabalhe com a qualidade e conceito dos Móveis Maschieto!
                        </h2>
                        <a href="{{ route('nav.revenda') }}" class="button button-color button-large button-rounded">Entre em
                            contato</a>
                    </div>
                </div>
            </div>
        </div>



    </div>
</section>

@endsection
