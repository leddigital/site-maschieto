@extends('layouts.default')
@section('content')


<section id="content">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="heading-block">
                        <h2>{{ $revenda->title }}</h2>
                        <p>{{ $revenda->short_description }}</p>
                    </div>

                    {{-- <div class="text-about my-5">
                        <p></p>
                    </div> --}}


                    <div class="col-md-6 col-lg-12">
                        <form id="revendaForm" action="{{ route('send.mail') }}" method="post" name="revendaForm">
                            @csrf
                            <div class="form-wrapper mt-5">
                                <div class="form-row">
                                    <div class="col-12 col-lg-8">
                                        <div class="form-group">
                                            <label for="store">Nome da Loja<small>*</small></label>
                                            <input type="text" id="store" name="store"
                                                value="" class="sm-form-control border-form-control required" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="cnpj">CNPJ<small>*</small></label>
                                            <input type="text" id="cnpj" name="cnpj"
                                                value="" class="sm-form-control border-form-control required" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-wrapper mt-5">
                                <div class="form-row">
                                    <div class="col-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="estado">Estado<small>*</small></label>
                                            <select id="estado" name="estado"
                                                class="sm-form-control custom-select border-form-control">
                                                <option value="">-- Selecione um --</option>
                                                <option value="Acre (AC)">Acre (AC)</option>
                                                <option value="Alagoas (AL)">Alagoas (AL)</option>
                                                <option value="Amapá (AP)">Amapá (AP)</option>
                                                <option value="Amazonas (AM)">Amazonas (AM)</option>
                                                <option value="Bahia (BA)">Bahia (BA)</option>
                                                <option value="Ceará (CE)">Ceará (CE)</option>
                                                <option value="Distrito Federal (DF)">Distrito Federal (DF)</option>
                                                <option value="Espírito Santo (ES)">Espírito Santo (ES)</option>
                                                <option value="Goiás (GO)">Goiás (GO)</option>
                                                <option value="Maranhão (MA)">Maranhão (MA)</option>
                                                <option value="Mato Grosso (MT)">Mato Grosso (MT)</option>
                                                <option value="Mato Grosso do Sul (MS)">Mato Grosso do Sul (MS)</option>
                                                <option value="Minas Gerais (MG)">Minas Gerais (MG)</option>
                                                <option value="Pará (PA)">Pará (PA)</option>
                                                <option value="Paraíba (PB)">Paraíba (PB)</option>
                                                <option value="Paraná (PR)">Paraná (PR)</option>
                                                <option value="Pernambuco (PE)">Pernambuco (PE)</option>
                                                <option value="Piauí (PI)">Piauí (PI)</option>
                                                <option value="Rio de Janeiro (RJ)">Rio de Janeiro (RJ)</option>
                                                <option value="Rio Grande do Norte (RN)">Rio Grande do Norte (RN)</option>
                                                <option value="Rio Grande do Sul (RS)">Rio Grande do Sul (RS)</option>
                                                <option value="Rondônia (RO)">Rondônia (RO)</option>
                                                <option value="Roraima (RR)">Roraima (RR)</option>
                                                <option value="Santa Catarina (SC)">Santa Catarina (SC)</option>
                                                <option value="São Paulo (SP)">São Paulo (SP)</option>
                                                <option value="Sergipe (SE)">Sergipe (SE)</option>
                                                <option value="Tocantins (TO)">Tocantins (TO)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-8">
                                        <div class="form-group">
                                            <label for="city">Cidade<small>*</small></label>
                                            <input type="text" id="city" name="city"
                                                 class="sm-form-control border-form-control required" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-wrapper mt-5">
                                <div class="form-row">
                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <label for="name">Nome responsável<small>*</small></label>
                                            <input type="text" id="name" name="name"
                                                 class="sm-form-control border-form-control required" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <label for="phone">Telefone<small>*</small></label>
                                            <input type="text" id="phone" name="phone"
                                                class="sm-form-control border-form-control required" />
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-wrapper mt-5">
                                <div class="form-row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="email">E-mail<small>*</small></label>
                                            <input type="email" id="email" name="email"
                                                value="" class="sm-form-control border-form-control required" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="typeContact" value="revenda">

                            <div class="form-wrapper mt-5 pull-right">
                                <button class="button button-3d nomargin" type="submit"
                                    id="template-contactform-submit" name="template-contactform-submit"
                                    value="submit">Enviar Solicitação</button>
                            </div>

                        </form>
                    </div>


                    {{-- <h4 class="mb-3">Perguntas frequentes</h4>
                    <div class="accordion mb-5 accordion-border clearfix">
                        <div class="acctitle"><i class="acc-closed icon-question-sign"></i><i
                                class="acc-open icon-question-sign"></i>Pergunta 1</div>
                        <div class="acc_content clearfix">Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Assumenda, dolorum, vero ipsum molestiae minima odio quo voluptate illum
                            excepturi quam cum voluptates doloribus quae nisi tempore necessitatibus dolores
                            ducimus enim libero eaque explicabo suscipit animi at quaerat aliquid ex expedita
                            perspiciatis? Saepe, aperiam, nam unde quas beatae vero vitae nulla.</div>
                        <div class="acctitle"><i class="acc-closed icon-comments-alt"></i><i
                                class="acc-open icon-comments-alt"></i>Pergunta 2</div>
                        <div class="acc_content clearfix">Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Explicabo, placeat, architecto rem dolorem dignissimos repellat veritatis in
                            et eos doloribus magnam aliquam ipsa alias assumenda officiis quasi sapiente
                            suscipit veniam odio voluptatum. Enim at asperiores quod velit minima officia
                            accusamus cumque eligendi consequuntur fuga? Maiores, quasi, voluptates,
                            exercitationem fuga voluptatibus a repudiandae expedita omnis molestiae alias
                            repellat perferendis dolores dolor.</div>
                        <div class="acctitle"><i class="acc-closed icon-lock3"></i><i
                                class="acc-open icon-lock3"></i>Pergunta 3</div>
                        <div class="acc_content clearfix">Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Possimus, fugiat iste nisi tempore nesciunt nemo fuga? Nesciunt, delectus
                            laboriosam nisi repudiandae nam fuga saepe animi recusandae. Asperiores, provident,
                            esse, doloremque, adipisci eaque alias dolore molestias assumenda quasi saepe nisi
                            ab illo ex nesciunt nobis laboriosam iusto quia nulla ad voluptatibus iste beatae
                            voluptas corrupti facilis accusamus recusandae sequi debitis reprehenderit
                            quibusdam. Facilis eligendi a exercitationem nisi et placeat excepturi velit!</div>
                        <div class="acctitle"><i class="acc-closed icon-credit"></i><i
                                class="acc-open icon-credit"></i>Pergunta 4
                        </div>
                        <div class="acc_content clearfix">Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Possimus, fugiat iste nisi tempore nesciunt nemo fuga? Nesciunt, delectus
                            laboriosam nisi repudiandae nam fuga saepe animi recusandae. Asperiores, provident,
                            esse, doloremque, adipisci eaque alias dolore molestias assumenda quasi saepe nisi
                            ab illo ex nesciunt nobis laboriosam iusto quia nulla ad voluptatibus iste beatae
                            voluptas corrupti facilis accusamus recusandae sequi debitis reprehenderit
                            quibusdam. Facilis eligendi a exercitationem nisi et placeat excepturi velit!</div>
                    </div> --}}

                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-md-6 col-lg-12">
                            <div class="card bg-dark dark mb-5 pb-2 px-2">
                                <div class="card-body">
                                    <h3 class="mb-3 uppercase ls1">Conheça a Maschieto</h3>
                                    <p class="card-text text-white-50">Nossa indústria foi fundada em 1968 com o propósito de realizar
                                        o desejo das pessoas em encontrar móveis que completam ambientes com estilo, design e conforto.</p>
                                    <a href="{{ route('nav.sobre') }}" class="button button-3d button-rounded m-0">saiba mais</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

<!--================Contact Success and Error message Area =================-->
<div id="success" class="modal modal-message fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Mensagem enviada com sucesso</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Sua mensagem foi enviada com sucesso. Aguarde nosso contato pelo e-mail ou telefone informados.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modals error -->

    <div id="error" class="modal modal-message fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Sua mensagem não pode ser enviada</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Estamos enfrentando problemas técnicos.
                        Por favor tente novamente mais tarde ou contate-nos pelos telefones</p> <br>
                        <ul class="mt-2">
                            <li>{{ $informations->phone1 }}</li>
                            <li>{{ $informations->phone2 }}</li>
                            <li>{{ $informations->phone3 }}</li>
                        </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <!--================End Contact Success and Error message Area =================-->

@endsection
