@extends('layouts.default')
@section('content')

<section id="page-title" class="page-title-parallax page-title-dark"
    style="background-image: url('{{ asset('mainbanner/'.$informations->main_banner) }}'); background-size: cover; padding: 120px 0;"
    data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
    <div class="container clearfix">
        <h1>Produtos</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('nav.index') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Produtos</li>
        </ol>
    </div>
</section>

<section id="content" class="clearfix" style="overflow: visible">
    <div class="content-wrap notoppadding">
        <div class="container">
            <div class="card p-4 shadow" style="top: -60px;">
                <form action="car-listing.html#" method="post" class="nobottommargin">
                    <div class="row clearfix">
                        <div class="col-md-4 col-sm-6 col-12 mt-4 mt-md-0">
                            <label for="">Nome do produto</label>
                            <input class="form-control" placeholder="Digite para pesquisar..."></input>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12 mt-4 mt-md-0">
                            <label for="">Selecione a linha</label>
                            <select class="selectpicker form-control customjs" title="Linha" data-size="10"
                                data-live-search="true" multiple data-live-search="true" style="width:100%;">
                                <optgroup label="Linhas">
                                    <option value="Poltronas">Poltronas</option>
                                    <option value="Cadeiras">Cadeiras</option>
                                    <option value="Home">Home</option>
                                    <option value="Mesas">Mesas</option>
                                    <option value="Aparadores">Aparadores</option>
                                    <option value="Buffets">Buffets</option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12 mt-4 mt-md-0">
                            <label for="">Selecione o Produto</label>
                            <select class="selectpicker customjs form-control" data-size="10" data-live-search="true"
                                title="Produto" style="width:100%; line-height: 30px;">
                                <optgroup label="Produto">
                                    <option value="R8">Camila</option>
                                    <option value="TT">Oskar</option>
                                    <option value="S5">Folha</option>
                                    <option value="A5">Asti</option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-6 col-6">
                            <button class="button button-3d button-rounded btn-block noleftmargin"
                                style="margin-top: 29px;">Procurar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="section nomargin pt-0 nobg">
            <div class="container clearfix">

                <ul class="portfolio-filter style-2 clearfix" data-container="#portfolio">
                    <li>
                        <a href="car-listing.html#" data-filter=".cf-bistro">
                            <span>Bistrô</span>
                        </a>
                    </li>
                    <li>
                        <a href="car-listing.html#" data-filter=".cf-buffet">
                            <span>Buffet</span>
                        </a>
                    </li>
                    <li>
                        <a href="car-listing.html#" data-filter=".cf-cadeira">
                            <span>Cadeira</span>
                        </a>
                    </li>
                    <li>
                        <a href="car-listing.html#" data-filter=".cf-aparador">
                            <span>Aparador</span>
                        </a>
                    </li>
                    <li>
                        <a href="car-listing.html#" data-filter=".cf-mesa">
                            <span>Mesa</span>
                        </a>
                    </li>
                    <li>
                        <a href="car-listing.html#" data-filter=".cf-banqueta">
                            <span>Banqueta</span>
                        </a>
                    </li>
                    <li>
                        <a href="car-listing.html#" data-filter=".cf-sofa">
                            <span>Sofá</span>
                        </a>
                    </li>
                    <li>
                        <a href="car-listing.html#" data-filter=".cf-poltrona">
                            <span>Poltrona</span>
                        </a>
                    </li>
                    <li>
                        <a href="car-listing.html#" data-filter=".cf-home">
                            <span>Home</span>
                        </a>
                    </li>
                    <li>
                        <a href="car-listing.html#" data-filter=".cf-rack">
                            <span>Rack</span>
                        </a>
                    </li>
                    <li>
                        <a href="car-listing.html#" data-filter=".cf-bar">
                            <span>Bar</span>
                        </a>
                    </li>
                    <li>
                        <a href="car-listing.html#" data-filter=".cf-banco">
                            <span>Banco</span>
                        </a>
                    </li>
                    <li>
                        <a href="car-listing.html#" data-filter=".cf-mesa-centro">
                            <span>Mesa de centro</span>
                        </a>
                    </li>
                    <li class="fright activeFilter">
                        <a class="button button-small button-rounded button-reset" href="car-listing.html#"
                            data-filter="*">Mostrar Todos</a>
                    </li>
                </ul>
                <div class="clear"></div>

                <div id="portfolio" class="portfolio portfolio-3 grid-container clearfix" data-layout="fitRows">

                    <!-- BISTRÔS -->

                    <article class="portfolio-item cf-bistro" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/bistro_ipanema.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Bistrô Ipanema</a></h3>
                        </div>
                    </article>

                    <!-- BUFFETS -->

                    <article class="portfolio-item cf-buffet" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/base_cad_buffet_prisma.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Buffet Prisma</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-buffet" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/buffet_xapuri.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Buffet Xapuri</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-buffet" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/buffet_galli.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Buffet Galli</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-buffet" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/buffet_trend.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Buffet Trend</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-buffet" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/buffet_carla.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Buffet Carla</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-buffet" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/buffet_riviera.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Buffet Riviera</a></h3>
                        </div>
                    </article>

                    <!-- CADEIRAS -->

                    <article class="portfolio-item cf-cadeira" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/cad_mirella.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Cadeira Mirella</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-cadeira" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/cad_dora.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Cadeira Dora</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-cadeira" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/cad_simone.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Cadeira Simone</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-cadeira" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/cad_camila.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Cadeira Camila</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-cadeira" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/cad_fernanda.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Cadeira Fernanda</a></h3>
                        </div>
                    </article>

                    <!-- APARADORES -->

                    <article class="portfolio-item cf-aparador" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/aparador_xapuri.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Aparador Xapuri</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-aparador" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/aparador_camila.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Aparador Camila</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-aparador" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/aparador_murano.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Aparador Murano</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-aparador" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/aparador_maisa.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Aparador Maisa</a></h3>
                        </div>
                    </article>

                    <!-- MESAS -->

                    <article class="portfolio-item cf-mesa" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/mesa_lyra.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Mesa Lyra</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-mesa" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/mesa_garden.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Mesa Garden</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-mesa" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/mesa_kelly.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Mesa Kelly</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-mesa" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/mesa_lorenza.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Mesa Lorenza</a></h3>
                        </div>
                    </article>

                    <!-- BANQUETAS -->

                    <article class="portfolio-item cf-banqueta" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/banq_natalia.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Banqueta Natália</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-banqueta" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/banq_fernanda.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Banqueta Fernanda</a></h3>
                        </div>
                    </article>

                    <!-- SOFÁS -->

                    <article class="portfolio-item cf-sofa" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/sofa_folha_3l.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Sofá Folha 3 lugares</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-sofa" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/sofa_folha_2l.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Sofá Folha 2 lugares</a></h3>
                        </div>
                    </article>

                    <!-- POLTRONAS -->

                    <article class="portfolio-item cf-poltrona" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/pont_folha.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Poltrona Folha</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-poltrona" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/pont_deise.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Poltrona Deise</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-poltrona" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/pont_anny.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Poltrona Anny</a></h3>
                        </div>
                    </article>

                    <!-- HOMES -->

                    <article class="portfolio-item cf-home" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/home_oskar.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Home Oskar</a></h3>
                        </div>
                    </article>

                    <!-- RACKS -->

                    <article class="portfolio-item cf-rack" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/rack_xapuri.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Rack Xapuri</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-rack" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/rack-painel_trend.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Rack Trend</a></h3>
                        </div>
                    </article>

                    <!-- BARES -->

                    <article class="portfolio-item cf-bar" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/bar_galli.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Bar Galli</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-bar" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/ap-bar_dora.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Bar Dora</a></h3>
                        </div>
                    </article>

                    <!-- BANCOS -->

                    <article class="portfolio-item cf-banco" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/banco_fernanda.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Banco Fernanda</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-banco" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/banco_fernnada_3l.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Banco Fernanda 3 lugares</a></h3>
                        </div>
                    </article>

                    <!-- MESAS DE CENTRO -->

                    <article class="portfolio-item cf-mesa-centro" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/mesa_centro_murano.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Mesa de Centro Murano</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-mesa-centro" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/mesa_centro_sila.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Mesa de Centro Sila</a></h3>
                        </div>
                    </article>

                    <article class="portfolio-item cf-mesa-centro" style="padding-bottom: 40px">
                        <div class="portfolio-image">
                            <a href="{{ route('nav.produto') }}">
                                <img src="images/produtos/mesa_centro_brasilia.jpg" alt="Open Imagination">
                            </a>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="{{ route('nav.produto') }}">Mesa de Centro Brasília</a></h3>
                        </div>
                    </article>

                </div>
            </div>
        </div>


        <div class="section nomargin footer-stick clearfix dark"
            style="background: url('images/background.jpg') left no-repeat; background-size: cover; padding: 120px 0">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="h2 t700 mb-4" style="color: #fff">Trabalhe com a qualidade e conceito dos Móveis Maschieto!
                        </h2>
                        <a href="{{ route('nav.revenda') }}" class="button button-color button-large button-rounded">Entre em
                            contato</a>
                    </div>
                </div>
            </div>
        </div>



    </div>
</section>

@endsection
