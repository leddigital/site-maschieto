<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Fale Conosco</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css?family=Open+Sans&display=swap');
        h5{
            margin-bottom: 0px;
            margin-top: 0px;
        }
    </style>
</head>

<body style="margin: 0; padding: 0; background:#eee;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 50px 0px;">
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="#fff">
                    <tr>
                        <td align="center" style="padding: 40px 30px 0px 30px;">
                            <table>
                                <tr>
                                    <td>
                                        <img src="{{ asset('images/logo.jpg') }}" alt="" width="150px"
                                            style="display: block;" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                            <table style="font-family: 'Open Sans', sans-serif;" border="0" cellpadding="0"
                                cellspacing="0" width="100%">
                                <tr>
                                    <td style="font-size: 18px;">
                                        <h1 style="font-size: 28px; text-align:center;">Contato Revendedor</h1>
                                        <br />
                                        <h5>Nome da Loja:</h5>
                                        {{ $store }}
                                        <br />
                                        <br />
                                        <h5>CNPJ:</h5>
                                        {{ $cnpj }}
                                        <br />
                                        <br />
                                        <h5>Estado:</h5>
                                        {{ $estado }}
                                        <br />
                                        <br />
                                        <h5>Cidade:</h5>
                                        {{ $city }}
                                        <br />
                                        <br />
                                        <h5>Nome Responsável:</h5>
                                        {{ $name }}
                                        <br />
                                        <br />
                                        <h5>Telefone:</h5>
                                        {{ $phone }}
                                        <br />
                                        <br />
                                        <h5>E-mail:</h5>
                                        {{ $email }}
                                        <br />
                                        <br />
                                    <td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; font-size:12px;">
                                        <p><strong>Enviado pelo formulário de revendedor.</strong></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
