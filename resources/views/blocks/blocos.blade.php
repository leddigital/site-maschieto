<div class="row m0  quick_block emmergency"
    style="-webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */filter: grayscale(100%);">
    <div class="row m0 inner">
        <div class="row heading m0">
            <h3>Fornecimento</h3>
        </div>
        <p> A experiência da Biocollagen permite que os produtos sejam fornecidos em mercados nacionais e
            internacionais.</p>
    </div>
</div>
<div class="row m0  quick_block branches"
    style="-webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */filter: grayscale(100%);">
    <div class="row m0 inner">
        <div class="row heading m0">
            <h3>Qualidade</h3>
        </div>
        <p>Todo trabalho é feito sob as normas do <br />ISO 13485 e seguindo as práticas ISO 22442.</p>
    </div>
</div>
<div class="row m0  quick_block bill_payments"
    style="-webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */filter: grayscale(100%);">
    <div class="row m0 inner">
        <div class="row heading m0">
            <h3>Rastreabilidade</h3>
        </div>
        <p>É construído um dossiê completo desde o nascimento até o abate do animal. O trabalho é realizado
            em sintonia com os matadouros.</p>
    </div>
</div>