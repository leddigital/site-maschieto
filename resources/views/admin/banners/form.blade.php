@extends('layouts.admin')
@section('title', 'Banners')
@section('content')
<header class="page-header">
    <h2>Banners</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('information.index') }}">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li><span>Cadastro de Banners</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col">
        <form class="form-horizontal form-bordered" id="form_cadastre" method="post"
            action="{{ isset($entity->id)?route('banners.edit.save',['id'=>$entity->id]):route('banners.save') }}"
            data-reload="{{ route('banners.index') }}">
            @csrf

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Textos no Banners</h2>
                    <p class="card-subtitle">
                        Preenchar as informações abaixo para exibir os textos dinâmicos de acordo com o banner.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-align-left"></i>
                                            </span>
                                        </span>
                                        <input value="{{ isset($entity->title)!=""?$entity->title:'' }}" type="text" name="title" class="form-control"
                                            placeholder="Título do banner" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-align-left"></i>
                                            </span>
                                        </span>
                                        <input value="{{ isset($entity->info1)!=""?$entity->info1:'' }}" type="text" name="info1" class="form-control"
                                            placeholder="Texto 1">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-link"></i>
                                            </span>
                                        </span>
                                        <input type="text" name="link1" value="{{ isset($entity->link1)!=""?$entity->link1:'' }}" class="form-control"
                                            placeholder="Link">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-align-left"></i>
                                            </span>
                                        </span>
                                        <input type="text" name="info2" value="{{ isset($entity->info2)!=""?$entity->info2:'' }}" class="form-control"
                                            placeholder="Texto 2">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-link"></i>
                                            </span>
                                        </span>
                                        <select id="inputState" class="form-control" name="textalign">
                                            <option value="top-left"
                                                {{ isset($entity->textalign)=="top-left"?"selected":"" }}>
                                                Esquerda, Acima
                                            </option>
                                            <option value="center-left"
                                                {{ isset($entity->textalign)=="center-left"?"selected":"" }}>
                                                Esquerda, Centro
                                            </option>
                                            <option value="bottom-left"
                                                {{ isset($entity->textalign)=="bottom-left"?"selected":"" }}>
                                                Esquerda, Abaixo
                                            </option>
                                            <option value="slider-caption-center"
                                                {{ isset($entity->textalign)=="slider-caption-center"?"selected":"" }}>
                                                Centro
                                            </option>
                                            <option value="top-right"
                                                {{ isset($entity->textalign)=="top-right"?"selected":"" }}>
                                                Direita, Acima
                                            </option>
                                            <option value="center-right"
                                                {{ isset($entity->textalign)=="center-right"?"selected":"" }}>
                                                Direita, Centro
                                            </option>
                                            <option value="bottom-right"
                                                {{ isset($entity->textalign)=="bottom-right"?"selected":"" }}>
                                                Direita, Abaixo
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="input-group">
										<div class="col-lg-12">
                                            <div class="input-group">
                                                <span class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fas fa-link"></i>
                                                    </span>
                                                </span>
                                                <input class="form-control" name="color" type="text" id="color"
                                                    value="{{ isset($entity->color)!=""?$entity->color:"" }}" placeholder="Cor do texto"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Banner</h2>
                    <p class="card-subtitle">
                        Selecione a image para a inserção de banner.<br />
                        <br />
                        <strong>Obs.: A imagem selecionada será automatimacamente reajustada para o tamanho na descrição
                            do campo.</strong>
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <small>Tamanho da imagem 1280px x 720px</small>
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input">
                                        <i class="fas fa-file fileupload-exists"></i>
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-exists">Trocar</span>
                                        <span class="fileupload-new">Selecionar Imagem</span>
                                        <input type="file" name='banner' accept="image/*"
                                            onchange='loadPreview(this, 1280,720)' {{ isset($entity)?'':'required' }}>
                                    </span>
                                    <a href="forms-basic.html#" class="btn btn-default fileupload-exists"
                                        data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <textarea id="base64" name="base64" style="display:none;"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="img-content">
                                <img id='output' class='img-fluid' src='{{ isset($entity->image)!=""?"/banners/".$entity->image:'' }}'>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Posição</h2>
                    <p class="card-subtitle">
                        Selecione onde o banner será posicionado.<br />
                        <br />
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="radio-custom radio-primary">
                                <input id="header_opt" name="position" type="radio" value="header" required {{ (isset($entity->position) and $entity->position=="header")?"checked":"" }} />
                                <label for="header_opt">Cabeçalho</label>
                            </div>
                            <div class="radio-custom radio-primary">
                                <input id="footer_opt" name="position" type="radio" value="footer" required {{ (isset($entity->position) and $entity->position=="footer")?"checked":"" }}/>
                                <label for="footer_opt">Rodapé</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
            </section>


            <section class="card">
                <div class="card-body" style="display: block;">
                    <button type="submit" class="mb-1 mt-1 mr-1 btn btn-success"><i class="fas fa-save"></i>
                        Salvar</button>
                </div>
            </section>
        </form>
    </div>
</div>
@endsection
