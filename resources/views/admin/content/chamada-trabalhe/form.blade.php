@extends('layouts.admin')
@section('title', 'Chamada Trabalhe Conosco | Admin Maschieto')
@section('content')

<header class="page-header">
    <h2>Chamada Contato</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('information.index') }}">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li><span>Chamada Trabalhe Conosco</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col">
        <form class="form-horizontal form-bordered" id="form_cadastre" method="post"
            action="{{ isset($entity->id)?route('content.edit.save',['id'=>$entity->id]):route('content.save') }}"
            data-reload="{{ route('content.form',['_type'=>$_type]) }}">
            @csrf
            <input type="hidden" name="type" value="{{ $_type }}">

            <h1>{{ $_type }}</h1>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Título da Chamada</h2>
                    <p class="card-subtitle">
                        Título da Chamada
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-align-left"></i>
                                            </span>
                                        </span>
                                        <input value="{{ isset($entity->title)!=""?$entity->title:'' }}" type="text"
                                            name="title" class="form-control" placeholder="Titulo" required>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="form-group row">
                                <label class="col-lg-3 control-label text-lg-right pt-2">Categorias</label>
                                <div class="col-lg-6">
                                    <select name="categories[]" multiple data-plugin-selectTwo
                                        class="form-control populate" id="select_categories">
                                        @foreach ($categories as $category)
                                        <option value="{{ $category->id }}"
                            {{ $category->contents[0]->pivot->categories_id==$category->id?'selected':"" }}>
                            {{ $category->title }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div> --}}
                </div>
    </div>
</div>
</section>

<section class="card">
    <header class="card-header">
        <div class="card-actions">
            <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
        </div>
        <h2 class="card-title">Descrição Curta</h2>
        <p class="card-subtitle">
            Breve descrição que irá abaixo do título.
        </p>
    </header>
    <div class="card-body" style="display: block;">
        <div class="form-group row">
            <div class="col-lg-12">
                <textarea name="short_description" class="form-control" rows="3">{{ isset($entity->short_description)?$entity->short_description:'' }}</textarea>
            </div>
        </div>
    </div>
</section>

<section class="card">
        <header class="card-header">
            <div class="card-actions">
                <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
            </div>
            <h2 class="card-title">Texto do link</h2>
            <p class="card-subtitle">
                insira o texto a ser exibido no link da chamada.
            </p>
        </header>
        <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <input value="{{ isset($entity->content)?$entity->content:'' }}" type="text" name="content"
                                    class="form-control" placeholder="Texto do link">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



<section class="card">
    <div class="card-body" style="display: block;">
        <button type="submit" class="mb-1 mt-1 mr-1 btn btn-success"><i class="fas fa-save"></i>
            Salvar</button>
    </div>
</section>


</form>
</div>
</div>



<script id="delete_gallery_template" type="x-tmpl-mustache">
    <input type="text" name="delete_gallery[]" class="form-control input-sm" placeholder="" value=" "/>
</script>
<script id="thumb_template" type="x-tmpl-mustache">
    <div class="gallery_item">
        <img src="@{{ thumb }}" class="img-responsive"/>
        <input type="hidden" name="gallery_image[]" value="@{{ original }}" />
        <input type="hidden" name="gallery_image_thumb[]" value="@{{ thumb }}" />
        <input type="text" name="gallery_image_description[]" class="form-control input-sm" placeholder="Descrição da imagem"/>
        <a href="javascript:;" class="" id="gallery_item_remove">
            <i class="mdi mdi-delete"></i>Remover
        </a>
    </div>
</script>

@endsection
