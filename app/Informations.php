<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Informations extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'address',
        'number',
        'complement',
        'district',
        'zipcode',
        'city',
        'state',
        'whatsapp',
        'instagram',
        'facebook',
        'linkein',
        'twitter',
        'pinterest',
        'opening',
        'email1',
        'email2',
        'phone1',
        'phone2',
        'phone3',
        'mailbox',
        'main_banner'
    ];
}
