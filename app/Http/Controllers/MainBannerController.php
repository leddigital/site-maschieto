<?php

namespace App\Http\Controllers;

use App\Informations;
use Illuminate\Http\Request;

class MainBannerController extends Controller {

    protected $model;

    public function __construct()
    {
        $this->model = new Informations();
    }

    public function index() {
        $entity = $this->model->get()->first();

        if(isset($entity->main_banner) && $entity->main_banner != "") {
            return view('admin.mainBanner.form', ['entity' => $entity]);
        } else {
            return view('admin.mainBanner.form');
        }
    }

    public function save(Request $request) {
        $form = $request->all();
        $destination_path = public_path() . '/mainbanner';

        if(!file_exists($destination_path)) {
            mkdir($destination_path, 0777);
        }

        if(isset($form['base64']) && ($form['base64'] != "")) {
            $image = $form['base64'];
            preg_match('/data:([^;]*);base64,(.*)/', $image, $matches);

            $type = explode('/', $matches[1]);
            $extension = $type[1];
            $filename = uniqid('mainbanner_') . '.' . $extension;

            $file = public_path() . '/mainbanner/' . $filename;
            $photobase = explode(',' , $image);
            $photo = base64_decode($photobase[1]);

            $form['image'] = $filename;

            $entity = new Informations();
            $c = $entity->get()->first();
            $old_file = $c->main_banner;

            $entity->get()->first()->update(array('main_banner' => $filename));

            file_put_contents($file, $photo);

            $res = [
                'status' => 200,
                'data' => $entity,
            ];

            return response()->json($res);
        }

    }

}
