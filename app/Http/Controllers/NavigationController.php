<?php

namespace App\Http\Controllers;

use App\Banners;
use App\Contents;
use App\Informations;
use App\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class NavigationController extends Controller
{

    public function __construct()
    {
        $categories = Categories::orderby('title', 'asc')->get()->all();
        View::share('categories', $categories);
    }

    public function index(Request $request)
    {

        $featuredCat = Categories::where('featured', '=', '1')->get();
        $featuredProd = Contents::where('featured', '=', '1')->get();

        $informations = Informations::get()->first();
        $banners = Banners::get()->all();
        $sobre = Contents::where('type', '=', 'sobre')->get()->first();
        return view('pages.index', [
            'banners' => $banners,
            'sobre' => $sobre,
            'informations' => $informations,
            'featuredCat' => $featuredCat,
            'featuredProd' => $featuredProd
            ]);
    }

    public function sobre(Request $request)
    {

        $informations = Informations::get()->first();

        $chamadaContato = Contents::where('type', '=', 'chamada-contato')->get()->first();
        $chamadaRevenda = Contents::where('type', '=', 'chamada-revenda')->get()->first();
        $chamadaTrabalhe = Contents::where('type', '=', 'chamada-trabalhe')->get()->first();
        $sobre = Contents::where('type', '=', 'sobre')->get()->first();

        return view('pages.sobre',
            [
                'sobre' => $sobre,
                'chamadaContato' => $chamadaContato,
                'chamadaRevenda' => $chamadaRevenda,
                'chamadaTrabalhe' => $chamadaTrabalhe,
                'informations' => $informations
            ]);
    }

    public function produtos(Request $request)
    {
        $informations = Informations::get()->first();
        $productList = Categories::with('contents')->get();

        return view('pages.produtos',[
                'informations' => $informations,
                'productList' => $productList
            ]);
    }

    public function produto(Request $request)
    {

        $url = $request->route('url');
        $produto = Contents::where('url', '=', $url)->get()->first();

        $informations = Informations::get()->first();

        return view('pages.produto', [
            'informations' => $informations,
            'produto' => $produto
        ]);
    }

    public function contato(Request $request)
    {
        $informations = Informations::get()->first();

        $contato = Contents::where('type', '=', 'contato')->get()->first();
        return view('pages.contato',
            [
                'contato' => $contato,
                'informations' => $informations
            ]);
    }

    public function revenda(Request $request)
    {

        $informations = Informations::get()->first();

        $revenda = Contents::where('type','=','revendedor')->get()->first();
        return view('pages.revenda',
            [
                'revenda' => $revenda,
                'informations' => $informations
            ]);
    }

    public function trabalhe(Request $request)
    {
        $informations = Informations::get()->first();
        $trabalhe = Contents::where('type', '=', 'trabalhe')->get()->first();

        return view('pages.trabalhe',
            [
                'trabalhe' => $trabalhe,
                'informations' => $informations

            ]);
    }

    public function services(Request $request)
    {
        $informations = Informations::get()->first();

        return view(
            'pages.services',
            [
                'informations' => $informations
            ]);
    }


}
