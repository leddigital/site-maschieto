<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Contents;
use App\ContentsImages;
use Illuminate\Http\Request;

class ContentsController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Contents();
    }

    public function index(Request $request)
    {
        //Pega o tipo de conteudo que vou gerenciar
        $_type = $request->route('_type');

        return view(
            'admin.content.' . $_type . '.index', ['_type' => $_type]
        );
    }

    //Listar todas as categorias
    public function readAll(Request $request)
    {
        $type = $request->route('_type');
        $collection = $this->model->where('type', '=', $type)->get()->all();

        $data['data'] = $collection;
        echo json_encode($data);
    }

    public function form(Request $request)
    {

        $_type = $request->route('_type');
        $type_category = 'tema';

        if($_type == "produto") {
            $id = $request->route('id');

        } else {
            $c = Contents::where('type', '=', $_type)->get()->first();
            $id = $c->id;

        }

        if (isset($id) and ($id != "")) {

            $entity = $this->model->with(['categories'])->find($id);
            $gallery = $entity->images()->get();

            $categories = Categories::with(['contents' => function ($q) use ($id) {
                $q->where('id', $id);
            }])->orderBy('title', 'ASC')
                ->get();

            return view('admin.content.' . $_type . '.form', [
                'entity' => $entity,
                'categories' => $categories,
                'gallery' => $gallery,
                '_type' => $_type
                // '_type' => 'produto',
            ]);

        } else {
            $categories = Categories::orderBy('title', 'ASC')->get();
            return view('admin.content.' . $_type . '.form', ['categories' => $categories, '_type' => $_type]);
        }
    }

    public function save(Request $request)
    {

        $folder = public_path() . '/content/';
        $form = $request->all();
        $id = $request->route('id');
        $_type = $form['type'];
        $byPassGalery = false;


        if($_type == "produto") {
            $id = $request->route('id');
        } else {
            $c = Contents::where('type', '=', $_type)->get()->first();
            $id = $c->id;

            if($_type != "sobre") {
                $byPassGalery = true;
            }
        }

        //Verifica se a pasta de conteudo já existe, se nao existir, a cria.
        if (!file_exists($folder)) {
            mkdir($folder, 0777);
        }

        //Inserir novo registro
        if (!isset($id) and $id == "") {

            //Gera a url amigável
            $form['url'] = $this->url_verify($form['title'], $this->model);

            $form['image'] = $this->saveImg($form['base64'], 'produto_', '/imgprodutos/');
            $form['descritivo'] = $this->saveImg($form['base64_2'], 'descritivo_', '/imgprodutos/descritivos/');

            $category = new Categories();

            // Fazer inserção do produto
            $entity = $this->model->create($form);

            if (isset($form['categories']) > 0){
                foreach ($form['categories'] as $category) {
                    $entity->categories()->attach($category);
                }
            }

            if ($entity) {

                $res = [
                    'status' => 200,
                    'data' => $entity,
                ];

            } else {
                $res = [
                    'status' => 500,
                    'data' => $entity,
                ];
            }

        } else {

            // //Atualizar o registro
            $entity = $this->model->find($id);

            // //Gera a url amigável
            $form['url'] = $this->url_verify($form['title'], $this->model, $id);

            
            if ($entity->update($form) && $byPassGalery == false) {

                //Gera um novo nome de arquivo
                if ((isset($form['base64']) and ($form['base64'] != "")) and ($form['remove_image_default']!=1)) {
                                    
                    if($form['type'] == 'sobre') {
                        // Se requisição for do tipo 'sobre'
                        $image_name = $this->saveImg($form['base64'], 'sobre_', '/content/'.$entity->id.'/', $entity->image);                        
                        $update['image'] = $image_name;
                    } else {
                        // Caso não for
                        $image_name = $this->saveImg($form['base64'], 'produto_', '/imgprodutos/', $entity->image);
                        $update['image'] = $image_name;
                    }
                    
                    $entity->update($update);
                }

                //Gera um novo nome de arquivo
                if ((isset($form['base64_2']) and ($form['base64_2'] != "")) and ($form['remove_image_default']!=1)) {
            
                    $descritivo_name = $this->saveImg($form['base64_2'], 'descritivo_', '/imgprodutos/descritivos/', $entity->descritivo);

                    $update['descritivo'] = $descritivo_name;
                    $entity->update($update);
                }

                if (isset($form['categories']) > 0) {
                    //Remove todas as categorias e adiciona novamente
                    $entity->categories()->detach();
                    //Adiciona novamente as categorias
                    foreach ($form['categories'] as $category) {
                        $entity->categories()->attach($category);
                    }
                }

                $res = [
                    'status' => 200,
                    'data' => $entity,
                ];
            }
        }

        if($byPassGalery == true) {
            $res = [
                'status' => 200,
                'data' => $entity,
            ];
        }

        return response()->json($res);
    }

    public function delete(Request $request)
    {
        $id = $request->route('id');
        $entity = $this->model->find($id);

        $path = public_path() . '/imgprodutos/';

        if ($entity->delete()) {
            @unlink($path . $entity->image);
            @unlink($path ."descritivos/". $entity->descritivo);
        }
    }
}
