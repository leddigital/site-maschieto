<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Contents;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Categories();
    }

    public function index(Request $request)
    {
        return view(
            'admin.categories.index'
        );
    }

    //Listar todas as categorias
    public function readAll(Request $request)
    {
        $collection = $this->model->get()->all();
        $data['data'] = $collection;
        echo json_encode($data);
    }


    public function form(Request $request)
    {
        $id = $request->route('id');

        if (isset($id) and ($id != "")) {
            $entity = $this->model->find($id);
            return view('admin.categories.form', ['entity' => $entity]);
        } else {
            return view('admin.categories.form');
        }
    }

    public function save(Request $request)
    {

        $form = $request->all();
        $id = $request->route('id');
        $destination_path = public_path() . '/categories';

        if(!isset($form['featured'])){
            $form['featured'] = 0;
        }

        if(!isset($destination_path)){
            mkdir($destination_path, 0777);
        }

        if(isset($form['base64']) && ($form['base64'] != "") && !isset($id) and $id == "") {

            $form['image'] = $this->saveImg($form['base64'], 'categoria_' ,'/categories/');

            //Validação de URL
            $form['url'] = $this->url_verify($form['title'], $this->model);

            //Fazer inserção do produto
            $entity = $this->model->create($form);

            $res = [
                'status' => 200,
                'data' => $entity
            ];

        }
        else {

            //Fazer update do registro
            $entity = $this->model->find($id);

            //Validação de URL
            $form['url'] = $this->url_verify($form['title'], $this->model, $entity->id);

            if(isset($form['base64'])) {
                $form['image'] = $this->saveImg($form['base64'], 'categoria_' ,'/categories/', $entity->image);
            }

            $entity = $entity->update($form);
            $res = [
                'status' => 200,
                'data' => $entity
            ];
        }
        return response()->json($res);
    }

    public function delete(Request $request){

        $destination_path = public_path() . '/categories';
        $id = $request->route('id');
        $entity = $this->model->find($id);

        @unlink($destination_path . '/' .$entity->image);
        $entity->delete();

    }

}
