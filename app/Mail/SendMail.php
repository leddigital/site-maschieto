<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($form,$subject)
    {
        $this->form = $form;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        // Se foi enviado do formulário de Contato ou de Revenda
        if($this->form['typeContact'] == "contato") {

            return $this
            ->from('contato@biocollagem.technology','Fale Conosco')
            ->subject("Fale Conosco | ".$this->subject)
            ->view('emails.contact',$this->form);

        } else if($this->form['typeContact'] == "revenda") {

            return $this
            ->from('contato@biocollagem.technology','Fale Conosco')
            ->subject("Contato Revendedor | ".$this->subject)
            ->view('emails.revenda',$this->form);

        }

    }
}
