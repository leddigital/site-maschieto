<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'image',
        'title',
        'info1',
        'info2',
        'link1',
        'path',
        'status',
        'order',
        'position',
        'color',
        'textalign'
    ];

    public $rules = [
        'image' => 'required',
        'title' => 'required',
    ];
    public $rules_update = [
        'title' => 'required',
    ];
}
