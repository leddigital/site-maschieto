<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Contents extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'title',
        'description',
        'short_description',
        'content',
        'image',
        'type',
        'url',
        'email_contato',
        'email_assunto',
        'video',
        'descritivo',
        'featured'
    ];

    public $rules = [
        'title' => 'required',
        'type'=>'required',
    ];

    public function images(){
        return $this->hasMany('App\ContentsImages');
    }

    public function categories(){
        return $this->belongsToMany('App\Categories','categories_has_contents', 'contents_id', 'categories_id');
    }
}
