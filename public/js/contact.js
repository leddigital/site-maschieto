jQuery.validator.addMethod("cnpj", function (value, element) {

    var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
    if (value.length == 0) {
        return false;
    }

    value = value.replace(/\D+/g, '');
    digitos_iguais = 1;

    for (i = 0; i < value.length - 1; i++)
        if (value.charAt(i) != value.charAt(i + 1)) {
            digitos_iguais = 0;
            break;
        }
    if (digitos_iguais)
        return false;

    tamanho = value.length - 2;
    numeros = value.substring(0, tamanho);
    digitos = value.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0)) {
        return false;
    }
    tamanho = tamanho + 1;
    numeros = value.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }

    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

    return (resultado == digitos.charAt(1));
})

jQuery.validator.addMethod("cpf", function (value, element) {
    value = jQuery.trim(value);

    value = value.replace('.', '');
    value = value.replace('.', '');
    cpf = value.replace('-', '');
    while (cpf.length < 11) cpf = "0" + cpf;
    var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
    var a = [];
    var b = new Number;
    var c = 11;
    for (i = 0; i < 11; i++) {
        a[i] = cpf.charAt(i);
        if (i < 9) b += (a[i] * --c);
    }
    if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11 - x }
    b = 0;
    c = 11;
    for (y = 0; y < 10; y++) b += (a[y] * c--);
    if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11 - x; }
    if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) return false;
    return true;
}, "Informe um CPF válido."); // Mensagem padrão








$(document).ready(function () {

    (function ($) {
        "use strict";


        jQuery.validator.addMethod('answercheck', function (value, element) {
            return this.optional(element) || /^\bcat\b$/.test(value)
        }, "type the correct answer -_-");

        // validate contactForm form
        $(function () {
            $('#contactForm').validate({
                rules: {
                    name: {
                        required: true,
                        minlength: 2
                    },
                    subject: {
                        required: true,
                        minlength: 4
                    },
                    phone: {
                        required: true,
                        minlength: 5
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    message: {
                        required: true,
                        minlength: 10
                    }
                },
                messages: {
                    name: {
                        required: "Por favor informe seu nome",
                        minlength: "Seu nome precisa conter ao menos 2 caracteres"
                    },
                    subject: {
                        required: "Por favor informe o assunto da sua mensagem",
                        minlength: "Seu assunto precisa conter ao menos 4 caracteres"
                    },
                    phone: {
                        required: "Por favor inform seu número de telefone para contato",
                        minlength: "Seu número de telefone deve conter ao menos 8 caracteres"
                    },
                    email: {
                        required: "Por favor informe seu e-mail"
                    },
                    message: {
                        required: "Sua mensagem não pode ser vazia",
                        minlength: "Sua mensagem não pode ter menos de 20 caracteres"
                    }
                },
                submitHandler: function (form) {
                    $(form).ajaxSubmit({
                        type: "POST",
                        // data: $(form).serialize(),
                        url: $(form).attr('action'),
                        success: function () {
                            $('#contactForm :input').attr('disabled', 'disabled');
                            $('#contactForm').fadeTo("slow", 1, function () {
                                $(this).find(':input').attr('disabled', 'disabled');
                                $(this).find('label').css('cursor', 'default');
                                $('#success').fadeIn()
                                $('.modal').modal('hide');
                                $('#success').modal('show');
                            })
                        },
                        error: function () {
                            $('#contactForm').fadeTo("slow", 1, function () {
                                $('#error').fadeIn()
                                $('.modal').modal('hide');
                                $('#error').modal('show');
                            })
                        }
                    })
                }
            })
        })

    })(jQuery)
})


$(document).ready(function () {

    (function ($) {
        "use strict";


        jQuery.validator.addMethod('answercheck', function (value, element) {
            return this.optional(element) || /^\bcat\b$/.test(value)
        }, "type the correct answer -_-");

        // validate contactForm form
        $(function () {
            $('#revendaForm').validate({
                rules: {
                    store: {
                        required: true,
                        minlength: 2
                    },
                    cnpj: {

                    }

                },
                messages: {
                    store: {
                        required: "Por favor informe o nome da sua loja",
                        minlength: "O nome da sua loja precisa conter ao menos 2 caracteres"
                    },

                },
                submitHandler: function (form) {
                    $(form).ajaxSubmit({
                        type: "POST",
                        // data: $(form).serialize(),
                        url: $(form).attr('action'),
                        success: function () {
                            $('#revendaForm :input').attr('disabled', 'disabled');
                            $('#revendaForm').fadeTo("slow", 1, function () {
                                $(this).find(':input').attr('disabled', 'disabled');
                                $(this).find('label').css('cursor', 'default');
                                $('#success').fadeIn()
                                $('.modal').modal('hide');
                                $('#success').modal('show');
                            })
                        },
                        error: function () {
                            $('#revendaForm').fadeTo("slow", 1, function () {
                                $('#error').fadeIn()
                                $('.modal').modal('hide');
                                $('#error').modal('show');
                            })
                        }
                    })
                }
            })
        })

    })(jQuery)

})




// $(document).ready(function () {

//             // validate contactForm form
//             $(function () {
//                     $('#revendaForm').validate({

//                             submitHandler: function (form) {
//                                 $(form).ajaxSubmit({
//                                     type: "POST",
//                                     // data: $(form).serialize(),
//                                     url: $(form).attr('action'),
//                                     success: function () {
//                                         $('#revendaForm :input').attr('disabled', 'disabled');
//                                         $('#revendaForm').fadeTo("slow", 1, function () {
//                                             $(this).find(':input').attr('disabled', 'disabled');
//                                             $(this).find('label').css('cursor', 'default');
//                                             $('#success').fadeIn()
//                                             $('.modal').modal('hide');
//                                             $('#success').modal('show');
//                                         })
//                                     },
//                                     error: function () {
//                                         $('#revendaForm').fadeTo("slow", 1, function () {
//                                             $('#error').fadeIn()
//                                             $('.modal').modal('hide');
//                                             $('#error').modal('show');
//                                         })
//                                     }
//                                 })
//                             }
//                         }

//                     )
//                 }

//             });
